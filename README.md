I think these current python scripts lack structure and modularity. There are a lot to improve!

# Global description
## Python files

The python files are:
- reconstruction: `reco.py`
- Recover the information of the runs: `load_runlist.py`
- Analysis software:
	- `read_analysis_alt.py`
	- `variables.py`
- Analysis of shower (using the analysis software)
	- `analysis_14.py`
	- `variables_14.py`
- Create pdf files of 1D histogram for different variables : `create_pdf_files.py`

## version
2 versions are defined:
- `version`: version of the Reconstruction
- `version_a`: version of the analysis

| version | version_a | Description |
| ------ | ------ |  ------ |
| 1.0 | 1.0 | 1.0 |
| 2.0 | 2.0 | `search_spatial_sigma_max` ($`\sigma_s`$) = 5, 10, 20, 30, ..., 70, -1 |
| 3.0 | 3.0 | `clu_std_u` computed with a gaussian fit |
| 3.0 | 3.1 | Cut in order to get only size-1 cluster upstream |
| 4.0 | 4.0 | `clu_std_v` corrected |
| 4.1 | 4.1 | A gap of 1 channel inside a cluster is allowed |
| 4.2 | 4.2 | Correction of hit\_fine\_value |
| 4.2 | 4.3 | Global cut instead of local cut |
| 4.2 | 4.7 | Cut \|residuals of tel 8\| < 0.01 |
| 5.0 | 5.0 | Radius-oriented reconstruction |
| 5.0 | 5.1 | Radius-oriented reconstruction and global cuts |

- [ ] Suggestion: we could merge version and version_a into version = a.b with a and b are too numbers:
	- a = version of the reconstruction
	- b = version of the analysis associated to the reconstruction

## Directories
There are 3 directories
- *scripts/*: contains all the python files
- *reconstructed_data/version* contains the reconstructed data for the version `version`
- *results/version* must contain the following folders to work properly
	- DATA/ - saved results in .txt
		- COMPARISON/ : if they combine several runs
		- SINGLE/ : if they are for one run
	- HDF5/[COMPARISON/ or SINGLE/] - contains the *.h5py* files of each run, that contains the features of every plots
	- PICKLE/ : the data of the runs (with the cuts) can be saved in *.pickle* in order to retrieve them faster
	- PLOTS:
		- COMPARISON/ : if they combine several runs, the plots are saved here
			- {considered_runs}/:
				- correlation: 2D histograms
				- all the 1D histograms
		- SINGLE/ : Single-run plots are saved here
			- 1400_w1011
			- ...
- raw_data/ where the raw_data is.


The basic repository *results/version* is available in the folder *blank_version* of this git repository.
It needs to exist before launching the analysis because I didn't create a function that create all the folders.

- [ ] Suggestion: Change the functions in order that they create the folder whenever it is needed

# Reconstruction of the runs 
## Important files
Analysis files:
- *analysis_14.toml*: this is the file by default with search_spatial_sigma_max = 5.0
- *analysis_14_sig10.toml*: search_spatial_sigma_max = 10.0
- *analysis_14_sig20.toml*
- ...

Device files:
- *device_lead_inside.toml*: 'scifi_stic' up to the plate 3 (included) and 'scifi_stic_shower' from plate 4 (included)
- *device_lead_inside_clu.toml*: 'scifi_stic_gap1' up to the plate 3 (included) to allow a gap of 1 channel in a cluster
- *device_lead_inside_ana.toml*: 
	- 'scifi_stic_gap1' up to the plate 3 (included) to allow a gap of 1 channel in a cluster
	- 'scifi_stic_shower_90' from plate 4 (included): radius-oriented definition of `clu\_u` and `clu\_std\_u` 



## Use reco-14.py to reconstruct runs (optimed for showers)

you might want to change **version** in the main().

| Goal | What to write | name of the run(s) |
| ------ | ------ |  ------ |
| Reconstruct the run 1011 | ./scripts/reco-14.py 1011 | '1011' |
| Reconstruct the run 1400 with the geometry files of 1011 (create them if they don't exist) | ./scripts/reco-14.py 1400 -r 1011 | '1400_w1011' |
| Reconstruct the run 1400 and 1401 with the geometry files of 1011 (create them if they don't exist) | ./scripts/reco-14.py 1400 1401 -r 1011 | '1400_w1011', '1401_w1011' |
| Reconstruct the run 1400 and 1401 with the geometry files of 1011 (create them if they don't exist), with $`\sigma_s\in\{5,10\}`$ | ./scripts/reco-14.py 1400 1401 -r 1011 -s 5 10 | '1400_w1011_sig5', '1400_w1011_sig10', '1401_w1011_sig5', '1401_w1011_sig10' |

There are two other parameters that you might want to use
- -rr (--run_ref_run): run the calibration and alignment of the reference run even if the geometry files already exists
- -rd (--redo): run the calibration and alignment of the considered even if the corresponding files already exist

If the reconstructed run is between 1400 and 1420, *analysis_14.toml* is used.

/!\ if:
- version == 4.1 or 4.2: device file used is "device_lead_inside_clu.toml"
- version == 5.0: device file used is "device_lead_inside_ana.toml"
You might want to remove this behaviour. You just need to comment out lines 53-58.

# Analysis of a run

In this part, I'll describe how to get plots from .root files.

## Get the information of the runs - load_runlist.py
I could not get the correct credential to access the google sheet.
I copied the sheet (*runlist_desy.csv*) and created *load_runlist.py* that works with this local .csv file.

When lauching the script,

	./scripts/load_runlist.py

two files are created:
- *runlist.csv*
- *rundict.pickle*, the informations of the runs are saved in a dictionnary `param_runs[{run}][{feature}]`, which is used in my analysis software, and loaded in *variables.py*

- [ ] Suggestion: Use the csv file rather than the pickle file in *variables.py*

The features of the runs stored in the the .pickle file are:
- 'overvoltage'   : overvoltage of the SiPms,
- 'energy'        : energy of the incoming beam,
- 'n_lead'        : number of leads,
- 'trigger_freq'  : trigger frequency


## Philosophy

1. `function run_analysis`: Convert run_\*\*\*\*-tree.root files for different runs given by the list *runs* into a python dictionnary `dTels_runs`
2. Create single-run single-tel histogram. The range, number of bins of the single plots have to be well-chosen, as they will be saved in the *.h5py* files. This is a boring task.
	a. function `hist`: 1D histogram for one variable
	b. function `hist_2D`: 2D histogram for variable [tel i] vs variable [tel j] OR variable1 [tel i] vs variable2 [tel i]
3. Put the histograms of several runs and tels in the same page, using the information stored in the .h5py files when generating the single plots
	a. function `hist_compar`: 1D histogram 
	b. function `hist_2D_compar`: 2D histograms
4. function `comparison`: Observe the evolution of the MPV/mean/... of some variable according to the runs

## variables.py
FIRST, we need to have a loot at *variables.py*
We might want to change:
- `version`
- `version_a`
- `local_cut`:
	- if True: each plate is considered separely
	- if False: if an event is cut out of 1 telescope, it is cut out of all the telescopes

The dictionnary `config_variables` contains the basic configuration of the plots for each variable
These configurations can be replaced by other ones when launching the functions that plot
The configurations are:

| Configurations | Description | Default value if not specified in this dictionnary (using the function `retrieve_config_variable`) |
| ------ | ------ | ------ |
| `'name_var'` | Alias name | Name of the variable in the software (clu\_value, ...) |
| `'y_log_scale'` | Y axis in log scale for the 1D (and 2D) histogram | False |
| `'fit'` | Gaussian fit | False |
| `'show_MPV'` | Show the MPV of the variable in the plot | False |
| `'same_y_max'` | Same range for the Y-axis when plotting 1D histograms for several runs and tels in the same page | True |

## Step 1: retrieve the .root data and store it into a dictionnary

Define a list of runs
 
	 runs = ['1403_w1011','1404_w1011','1405_w1011']

/!\ Do not exceed ~ 3-5 runs as the dictionnary stored in memory might get quite big.
I had to restart LPHE's computer because of that!

The function to use is
	dTels_runs,dTels_runs_tot  = runs_analysis(runs,  path_file = path_file, sigmas = [''],
    max_residual=1, min_cluster=0, max_1track =True, display = False,
    save = True, recover = True, local_cut = True)

With:
- save: if True, save dTel_runs and dTel_runs_tot in a pickle file in order to retrieve it faster afterwards
- retrieve: retrieve the file if it is available in a .pickle file
- display: display the number of events in every dictionnary and at each cut

For instance:

	dTels_runs,dTels_runs_tot = runs_analysis(runs, max_residual = 100, recover = True, save = True, display=True)


dTel_runs has undergone the cuts:
- |residuals| < max_residual
- cluster_size > min_cluster
- if max_1track: evt_ntracks == 1

dTel_runs_tot has undergone only evt_ntracks == 1 if max_1track

To access the variable 'clu_u' of the tel '1' of the run '1403_w1011':

	dTels_runs['1403_w1011']['1']['clu_u']

__
If we scanned `search_spatial_sigma_max` = {5,10} for the run '1403_w1011', to recontrust them using `runs_analysis`, we can use the argument: `sigmas = ['5','10']`.
Then, to access to the run 1403_w1011_sig5 in dTels_runs, we use the key `'1403_w1011|5'`.

## Step 2: Create the single plots

If not specified, the variables range_var, show_mean, show_MPV, y_log_scale and fit and retrieved in the dictionnary `config_variables` in *variables.py*

### 1D histogram

The function to use is:

	hist(dTels_runs,variable, sigmas = [''], name_var = None, tels = None, n_bins = 100, 
    range_var = None, show_mean = None, show_MPV = None, show_nevents_bins = None,
    y_log_scale = None, fit = None, mu0 = 0, sigma0 = None)

Some interesting parameters we did not talk about yet:
- range_var: range of the x-axis of the 1D histogram
	- if None:-> range [min,max] of the distribution of the considered variable
	- if [None, b]-> range [min,b]
	- if [a, None]-> range [min,None]
	- if [a, b] -> range [a,b]
- `show_nevents_bins` : show the number of events in every bin. if it is None, it is automatically True if the number of bins <= 10
- `mu0` and `sigma0`: are the parameters of the gaussian fit (if a fit is performed).
- `tels`: is the list of telescopes for which we want to plot the corresponding 1D histogram. If None, all the telescopes inside the dictionnary are plotted.
- name_var: alternative name of the variable. It will be used for the title, the name of the axis, and the file which will be saved (spaces are replaced by '_')

For instance, if we want to plot all the histograms of the variable `'clu_u'`, of the runs in dTels_runs, of all the telescopes, in the range [0,3000], for the downstream telescopes defined by tels = ['4','5', '6', '7', '8', '9', '10', '11'] (NB: there are str, not integer!!)

	hist(dTels_runs,'clu_u', tels = ['4','5', '6', '7', '8', '9', '10', '11'], range_var = [0,3000])

It is also possible to:
- write directly `'residuals'` for `variable` to get the residual distribution
- write `'a*clu_std_u'` to get a times `clu_std_u`. This was used to get the distribution of the shower radius.

It is also possible to perform a fit, passing the argument `fit = True`. The goodness of fit is the reduced $`R^2`$. Note that the function to compute the reduced $`\chi^2`$ already exists(see `reduced_chi2`). If you prefer this goodness of fit, it should be straightfoward to implement it.


The output is:
- The bin centers, bin contents, MPV and the mean of the distribution are saved in a .h5py file. The results of the fit are also saved.
- The figures are saved in results/{version_a}/PLOTS/SINGLE/{run}/



### 2D histogram

2 cases must be considered:
- **Case 1**: We want to plot the 2D histogram of 2 different variables
- **Case 2**: We want to plot the 2D histogram of 1 variable, but for 2 different telescope. I used this one in order to plot shower_radius in a X plane vs the one in a Y plane (of the same X-Y plane).

The function to use is:

	hist_2D(dTels_runs,variable,l_groups_tels, sigmas = [''],name_var = None, n_bins = 40,range_var = None, log_scale=None)


**Case 1**: 2D histogram of `'1.64485*clu_std_u'` and `'residuals'` for all the downstream telescopes

	hist_2D(dTels_runs, ['1.64485*clu_std_u','residuals'], ['4', '5', '6', '7', '8', '9', '10', '11'], name_var = ['shower radius',None])

Note that range\_var [range\_var1,range\_var2] is such that range\_var1 and range\_var2 have the same working principle as the one of the 1D histogram.
All the variables (name_var, n_bins and log_scale) are lists of 2 elements.
n_bins can be specified as integer if you want to define the same number of bins along the two axes of the 2D histogram.


**Case 2**: 2D histogram of `'1.64485*clu_std_u'` for tel5&tel4 and tel7&tel6

	hist_2D(dTels_runs,'1.64485*clu_std_u',[['5','4'],['7','6']], name_var = 'shower_radius')

range\_var has the same working principle as the one of the 1D histogram.

The figures are saved in results/{version_a}/PLOTS/SINGLE/{run}/correlation/

- [ ] Suggestion : it would be great and easy to implement the log scale for the color bar, especially for comparing different runs.

## Step 3: multi-runs and multi-telescopes plots
Now that the single plots have been generated in the previous part, we do not need dTels_runs anymore.
Then, the variables range\_var and n\_bins will not be specified.

### 1D histogram
The figures are saved in results/{version_a}/PLOTS/COMPARISON/{runs or runs_text}/

#### Not super-imposed
The function to use is:

	hist_compar(variable, group_runs,sigmas=[''], name_var = None, tels=None, same_y_max = None, 
    show_mean = None, show_MPV = None, show_nevents_bins = None, show_1tick = True,fit=None,
    y_log_scale = False,norm = None, max_tels = 4, index_color = 0,
    group_variable = None, sorting_variable = None, runs_text = None)

New arguments have been implemented. Let's forget about `group_variable` `sorting_variable` which will be explained in the next part ("sort the runs").
- show_1tick: if True, only show the ticks of the last row of plots as for the X-axis.  This is useful when the variables have the same range from one telescope to another (for instance, all the upstream telescopes).
	- [ ] Suggestion: we could compute the range of every variable and set this variable automatically, if they have indeed the same range.
- max_tels = 4: if more than 4 telescopes are specified (i.e., len(tels) > 4), plot odd and even telescopes in 2 different images.
- index_color: index that indicates the color of the plot. if more than {max_tels} telescopes are specified and we need 2 different images, then the index\_color is automatically incremented for the next page, so that the histograms of the next page has a different color.
- norm: if True, normalise the graphs. Automatically does that if we want to super-impose the runs
- runs_text: alternative name of the run. The plot will be saved in the folder {runs_text} 

The template of the plot is the following:

                   	run 1   run 2   ...
            Tel 0
            Tel 1
            ...

For instance, if we want to plot the variable 'clu_u', for the runs `runs = ['1403_w1011','1404_w1011','1405_w1011']`, for the telescopes after the lead (`['4', '5', '6', '7', '8', '9', '10', '11']`), with index\_color = 1 (so that it is different from the color of the histograms in the upstream tels), and I want it saved in a folder called 'nlead1'

	hist_compar('clu_u', ['1403_w1011','1404_w1011','1405_w1011'], ['4', '5', '6', '7', '8', '9', '10', '11'], index_color = 1, runs_text = 'nlead1')

Then, the Y and X SciFi planes are automatically plotted in two different pages (thanks to the odd-even repartition of the telescopes in the two pages)

#### Super-imposed
To super-impose the runs in the same plot, we need to specify a list of list of runs instead of a list of runs.
For instance, tu superimpose the runs '1403_w1011','1404_w1011' and '1405_w1011'

	hist_compar('clu_u', [['1403_w1011','1404_w1011','1405_w1011']], ['4', '5', '6', '7', '8', '9', '10', '11'], index_color = 1, runs_text = 'nlead1')


This is very handy because if we want to super-impose '1403_w1011','1404_w1011','1405_w1011', and, next to it, '1404_w1011','1405_w011','1406_w1011', we can directly write:

	hist_compar('clu_u', [['1403_w1011','1404_w1011','1405_w1011'],['1404_w1011','1405_w011','1406_w1011']], ['4', '5', '6', '7', '8', '9', '10', '11'], index_color = 1, runs_text = 'nlead1')

A '_SI' is added to the name of the image.

### 2D histogram

The function to use is:
	hist_2D_compar(variables, runs,l_groups_tels, sigmas=[''], name_var = None, log_scale = None, same_max = None, 
    index_color = 0, group_variable = None, sorting_variable = None, runs_text = None, max_tels=4)

As you can see, this is very similar to hist_compar (there is the same system of 2 pages if max\_tels is exceeded, the same system of color, ...)
There is a new argument though: `same\_max` is
- if there is one variable (and 2 telescopes), a Boolean
- if there are two variables (and 1 telescope), a list of 2 Booleans.
If the Boolean is True, then the ticks of the corresponding axis (X or Y) are shown only for all the plots of the first row (for the Y axis) or last column (for X axis) (because the range is supposed to be the same for all the other plots).

Note that a True value of log_scale for one of the variable created errors when plotting. I don't know why.

The template of the plot is like this:
- If there is only 1 variable and l_groups_2Tels is a list of group of 2 telescopes:


                                		runs[0]                         runs[1]             ...
    	variable l_groups_2Tels[0][0]
                                    	variable l_groups_2Tels[0][1]
    	variable l_groups_2Tels[1][0]
                                    	variable l_groups_2Tels[1][1]    
		...


- If variable is a list of 2 variables and l_groups_2Tels is a list of telescopes:


                                        runs[0]                         runs[1]             ...
    	variable[1] l_groups_2Tels[0]
                                        variable[0] l_groups_2Tels[0]
    	variable[1] l_groups_2Tels[1]
                                        variable[0] l_groups_2Tels[1]    
		...



For instance, to plot `'1.64485*clu_std_u'` vs `'residuals'`, for `tels = ['4','5','6','7','8','9','10','11']` and runs `'1403_w1011'`,`'1404_w1011'` and `'1405_w1011'`

	hist_2D_compar(['1.64485*clu_std_u','residuals'],['1403_w1011','1404_w1011','1405_w1011'], ['4','5','6','7','8','9','10','11'], runs_text='nlead1', index_color=1)

In order to plot `'1.64485*clu_std_u'`, for telescope 5&4 and 7&6,  and runs `'1403_w1011'`,`'1404_w1011'`,`'1405_w1011'`

	hist_2D_compar('1.64485*clu_std_u',['1403_w1011','1404_w1011','1405_w1011'], [['5','4'],['7','6']], runs_text='nlead1', index_color=1)

The figures are saved in results/{version_a}/PLOTS/COMPARISON/{runs or runs_text}/correlation

## Sort the runs

### SORTING

Suppose that all the runs have been reconstructed ('1403_w1011','1404_w1011','1405_w1011', '1406_w1011','1407_w1011','1408_w1011','1409_w1011','1410_w1011','1411_w1011','1412_w1011','1413_w1011','1414_w1011','1415_w1011','1416_w1011','1417_w1011','1418_w1011','1419_w1011','1420_w1011')

We might want to sort:
- Group them in runs of same `'energy'`
- Sort the these groups
- Within each group, sort the runs by `'n_lead'` (number of leads)

The function to use is:
	get_group_runs(runs,variable,sorted = True,sort_variable = None)

With:
- runs: list of runs or list of list of runs
- variable: variable that is used to sort the runs
    - `'n_lead'`: number of leads
    - `'trigger_freq'`: frequency of the trigger
    - `'energy'` : energy of the beam
- if sorted == True: the groups are sorted into ascending order of the value of {variable} (uses an insertion sort)
- if sort_variable is not None: the runs inside every group of runs is sorted in the ascending order of {sort_variable}

Note that the function work for other grouping and sorting variables (in theory)

For instance, to do what I wrote at the beginning:

	groups_runs = [
		['1403_w1011','1404_w1011','1405_w1011'],
		['1406_w1011','1407_w1011','1408_w1011'],
		['1409_w1011','1410_w1011','1411_w1011'],
		['1412_w1011','1413_w1011','1414_w1011'],
		['1415_w1011','1416_w1011','1417_w1011'],
		['1418_w1011','1419_w1011','1420_w1011']
	]
	groups_runs = get_group_runs(groups_runs,'energy',sorted = True,sort_variable='n_lead')


### 1D and 2D histograms

Suppose the runs are (already) sorted as specified in the previous section. Then, we can
- write the energy/# lead plates of every run in the plots,
- save the images by group of energy (for instance, 2.0, 4.0 and 5.6), with, for instance, '_energy2.0' in the name of the plot.

using the variables `group_variable = 'energy'` and `sorting_variable = 'n_lead'` of hist_2D_compar and hist_compar, like this:


	hist_2D_compar(['1.64485*clu_std_u','clu_u'],group_runs, [4,5,6,7,8,9,10,11], runs_text='with_lead', index_color=1, group_variable = 	'energy', sorting_variable = 'n_lead')

	hist_compar(variable, groups_runs, ['4', '5', '6', '7', '8', '9', '10', '11'], index_color = 1, runs_text = 'withl_lead", group_variable = 'energy', sorting_variable = 'n_lead')


## Plot the MPV/mean/... of a variable vs the # lead plates or the runs

We want to plot the MPV/mean/max/sigma of some variable (sigmas works only for the variables whose distribution is fitted) as a function of: there are 2 cases
- The runs (in abscissa) --> This can be used for all the DESY runs.
- the number of leads (if the argument `with_leads = True` in the function below). In this case, automatically, the energy of the runs are retrieved and sorted, and for a given plane, a lighter color corresponds to a lower energy.

/!\ Except for `'max'` of the variable, the single 1D histogram of the considered variable must have been generated first (in order to save the MPV/mean/sigma in the .h5py)

The function to use is:

	comparison(runs,variable,tels = [0], char = 'MPV', sigmas = [''], with_leads = True,show_runs=False, name_runs = None)

- runs: runs to compare
- variable: what variable are we going to compare for different runs
- tels: for what tels (1 curve = 1 telescope)
- char: considering the distribution of the variable, we going to compare
    * `'MPV'`: Most Probable Value of the distribution of the variable
    * `'mean'` : Mean value of the distribution of the variable
    * `'sigma'` : sigma of the gaussian fit of the considered variable
    * `'max'`
- if with_leads = True, instead of having runs in x-axis, we'll have "# leads".
	In this case, if show_runs = True, all the names of the runs corresponding to a #lead and an energy, are are indicated below the figure.
	This is very quite ugly.
	Without this, the number of runs is never written (if with_leads = True). 

For instance, if we want to show the MPV of `'clu_value'` as a function of the number of runs, for the telescopes `['4', '6', '8', '10']`, we would write:

	list_runs = list(np.array(groups_runs).flatten()) # we flatten the list
	comparison(list_runs,'clu_value',tels = ['4', '6', '8', '10'], char = 'MPV',  name_runs = 'with_lead')


## Other (probably useless now) functions

### Number of events as a function of search_spatial_sigma_max

	analysis_number_events(dTels_runs_tot,list_tels)

The function  was implemented to compare the number of events in the reconstructed runs with different search_spatial_sigma_max.
You'll probably not need it.

The function:
- Print the table of the number of events in dTels_runs_tot for each run and tel and save in in .txt in result/DATA/SINGLE/ for each run.
- Plot the number of events for each run and telescope, as a function of search_spatial_sigma_max, like this:
	    	           Run 1                                       Run 2   ...
        Tel 0     	number_events as a function of sigma
        Tel 1
        ...


### Total efficiency

	total_efficiency(dTels_runs, dTels_runs_tot)

The function writes the efficiency of every run and telescopes in the dictionnary, and plot the efficiencies, for each telescope, as a function of the run.

### Efficiency of the channels

	efficiency_channels(dTels_runs, dTels_runs_tot)

It would allow to get the the efficiency of each channel, as Ettore asked for during the first weeks.
It would plot these efficiencies, for each run and telescope, as a function of the channel.
It also save the result into an .h5py file.

# Analyse of showers using variables_14.py and analysis_14.py

In order to facilitate the analysis of the runs 1400-1420, two files *variables_14.py* and *analysis_14.py* have been created, containing functions to runs the functions mentionned before.

To launch an analysis of the shower, I'll just need to write:

	./scripts/analysis_14.py

As we will explain later on, *variables_14.py* contains among other things, all the expected variables of the variables for plotting, i.e., the **range** and the **number of bins** to use for the histograms of a given variable, that before or after the lead.


## Functions in analysis_14.py

We define two functions that allow to retrieve the range and #bins of the variables (in *variables_14.py*) in order to plot the histogram.

To create the single histograms:

	analysis_variable_single(variable,dTels_runs,runs,tels_before=None,tels_after=None,version_a = va.version_a,sigmas = None)

It creates the single-run single-telescope histogram of the variable {variable}, for the telescopes before and after the lead (if specified)
- the telescopes before the lead are specified by `'tels_before'`
- the telescopes after the lead are specified by `'tels_after'`
- the version of the run is already given by default in *variables.py*, imported as `'va'`

To create the plots with several 1D histograms, for a set of runs and telescopes, specified by tels_before and tels_after:

	analysis_variable_global(variable,runs,tels_before=None,tels_after=None,version_a = va.version_a,sigmas = None,
    group_variable = None, sorting_variable = None, runs_text=None)

In order to launch the full analysis, I use:

	launch_analysis(groups_runs, path_file,variables={}, tels_before = None, tels_after = None, scatter_variables={},l_groups_2Tels=[],  version = va.version, version_a = va.version_a,sigmas = None, class_variable = None, sorting_variable = None, runs_text = None, local_cut= False)

It allows to:
- Create the single plots of 1D/2D histogram, if wanted
- Create the big plots of 1D/2D histograms, which combine several runs and telescopes. I class_variable and sorting_variable are specified, the runs are even sorted.
- Create the superimposed plots if the runs are grouped by `class_variable`

I use this function in order to create the plots of MPV/mean/... of a variable as a function of the # lead plates.

	launch_comparison_variables(list_runs, comparison_variables,sigmas)

I rarely use those 2 functions, because I often choose what I want to plot using directly *variables_14.py*




## Chosing what you want to plot using *variables_14.py*

First, the list of runs are specified by `groups_runs`. It should be a list of list.
Indeed, when we need to create a single plots, the files will be loaded in groups, specified in `groups_runs`, in order to avoid to load all the runs at the same time.
For instance, for:

	groups_runs = [
		['1403_w1011','1404_w1011','1405_w1011'],
		['1406_w1011','1407_w1011','1408_w1011'],
		['1409_w1011','1410_w1011','1411_w1011'],
		['1412_w1011','1413_w1011','1414_w1011'],
		['1415_w1011','1416_w1011','1417_w1011'],
		['1418_w1011','1419_w1011','1420_w1011']
	]

The analysis software with create `dTels_runs` for ['1403_w1011','1404_w1011','1405_w1011'] and create all the associated single plots, then it will drop it and create another `dTels_runs` for ['1406_w1011','1407_w1011','1408_w1011'], and so on so forth.



Secondly, the list of tels are specified:
- The lists `tels_before` and `tels_after` (before/after the lead) are specified. They are used to plot the **1D histograms** and the **2D histograms with 2 different variables** and 1 telescope.
If you don't want to plot one of them, just set it at None.
- `l_groups_2Tels` specifies the list of list of 2 telescopes in order to plot the **2D histogram of one variable** (for instance, `'1.64485*clu_std_u'`) for 2 telescopes X-Y
- `group_tels` is used for the function `comparison`, where we plot the **MPV/mean/sigma/... of a variable as a function of the runs or of the # lead plates**. It must have the structure of "list of list"

Then, the list `sigmas` of search_spatial_sigma_max is defined. Just let it at ['']

Then, the dictionnary `variables` controls the 1D histograms we want to plot.
If we want to plot the big plot of 1D histograms associated to 'clu_u', we would write:

	variables = { # {variable}:{do_we_need_to_create_the_single_plot}
		'clu_u':False
	}

If we want to create all the associated single plots, and write again in the .h5py files, we would write:

	variables = { # {variable}:{do_we_need_to_create_the_single_plot}
		'clu_u':True
	}

Then, `scatter_variables` is the same, but for 2D histograms.
In order to plot the 
- 2D histogram of `'1.64485*clu_std_u'`, with groups of two telescopes specified by l_groups_2Tels, and we want to re-generate the single plots
- 2D histogram of residuals vs clu_std_u

we would write:

	scatter_variables = {# {variable(s)}:{do_we_need_to_create_the_single_plot}
		'1.64485*clu_std_u': True,
		('residuals','clu_std_u'): False,
	}

Then, to plot the plots "MPV/mean/... of variable as a function of the # lead plates", we use the dictionnary `comparison_variables`.
For instance, the following dictionnary plots (as a function of the # lead plates):
- the sigma of the residuals for the telescopes specified in `group_tels`
- the MPV of the trk_chi2, but only for the first telescope (since the trk_chi2 is global, it does not change with the telescope. It would be better to plot it just for 1 telescope)

	comparison_variables = {
		'residuals' : {
			'param':'sigma',
			'groups_tels': group_tels,
		},
		'trk_chi2':{
			'param':'MPV',
			'groups_tels':['0']
		}
	}


Then, there is the very useful function:
	
	get_config_plot(variable,runs,version_a)

For a given:
- variable
- list of runs
- version of the analysis

it returns: 
 - range_b, range_a: the range (min,max) of the corresponding variable before/after the lead
 - n_bins_b, n_bins_a: the number of bins of the histogram of the corresponding variable before/after the lead

If you define a new run, or a new version, you must change this function and set the range_b/range_a/n_bins_b/n_bins_a to the corresponding wanted value, except for clu\_u, clu\_channel and hit\_channel that never need to change.

# Visualisation of the events of a given run

This function allows, for a given run (and a set of telescopes) to visualise the hits of the events in the specified telescopes.
The function to use is:

	hit_visualisation(dTels, tels = ['5', '4', '7', '6', '9', '8', '11', '10'], starting_event = 0,end_event=50, name_run = None)

With:
- dTels: for a given run, dTels is a dictionnary such that dTels[tel] allows to access to the data of the telescope {tel}
- tels: list of telescopes
- name_run: name of the run (for the folder in which it will be saved)
- starting_event: from what number of event we start
- end_event: from what number of event we end

It saves the result as a PDF, in results/version_a/visualisation/{name_run}/

However, you can easily uncomment "plt.show()" and comment out "pp.savefig(fig,bbox_inches='tight')" in order to visualise directly the result without saving it.

For instance, if we want to analyse the first 50 events of the run '1409_w1011':

	dTels_runs,_ = runs_analysis(['1409_w1011'], max_residual = 100, recover = True,save = True, display=True)
	hit_visualisation(dTels_runs['1409_w1011'], tels = ['5', '4', '7', '6', '9', '8', '11', '10'], starting_event = 0,end_event=50, name_run = None)

You can also use the function `hit_visualisation_run(run)` in *analysis_14.py*

# The end
There are other features implemented in some functions, that I did not talk about.
Please feel free to read the comments in the functions!
I'm already sorry for some parts of the code that I did not comment enough.

There is also a examples.py in scripts, with some lines I wrote in this tutorial (I used it to test the functions of this documentation)


