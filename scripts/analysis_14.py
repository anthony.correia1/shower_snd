#!/usr/bin/env python3

import sys
import os
#sys.path.append(os.path.abspath("/home/ancorrei/scifi/"))
import os.path as op
import read_analysis_alt as ra
import variables as va
from variables_14 import *
import argparse
import numpy as np

### ======================================== FUNCTIONS TO CREATE THE PLOTS ======================================== ###


def analysis_variable_single(variable,dTels_runs,tels_before=None,tels_after=None,version_a = va.version_a,sigmas = None):
    """
    Create single plots for the runs in dTels_runs and telescopes 
    """
    runs,_,_ = ra.get_keys(dTels_runs)
    range_b, range_a, n_bins_b, n_bins_a = get_config_plot(variable,runs,version_a)
    # name_var = config_variables[variable]['name_var']
    # y_log_scale = config_variables[variable]['y_log_scale']
    # show_MPV = config_variables[variable]['show_MPV']
    # fit = config_variables[variable]['fit']
    if tels_before is not None:
        ra.hist (dTels_runs,variable,tels=tels_before, range_var = range_b, n_bins=n_bins_b)
    if tels_after is not None:
        ra.hist (dTels_runs,variable,tels=tels_after, range_var = range_a, n_bins=n_bins_a)
    
 
    
def analysis_variable_global(variable,runs,tels_before=None,tels_after=None,version_a = va.version_a,sigmas = None,
    group_variable = None, sorting_variable = None, runs_text=None):
    """
    Create for the telescopes before and after the plates, pages 
    """
    # name_var = config_variables[variable]['name_var']
    # y_log_scale = config_variables[variable]['y_log_scale']
    # show_MPV = config_variables[variable]['show_MPV']
    # same_y_max = config_variables[variable]['same_y_max']

    if tels_before is not None:
        ra.hist_compar(variable, runs,tels=tels_before, sigmas = sigmas, index_color = 0,
            group_variable=group_variable, sorting_variable=sorting_variable,runs_text = runs_text)
    if tels_after is not None:
        ra.hist_compar(variable, runs,tels=tels_after,sigmas = sigmas, index_color = 1,
            group_variable=group_variable, sorting_variable=sorting_variable, runs_text = runs_text)


## Function to launch the analysis -------------------------------
def launch_analysis(groups_runs, path_file,variables={}, tels_before = None, tels_after = None, scatter_variables={},l_groups_2Tels=[], version = va.version, version_a = va.version_a, 
    sigmas = None, class_variable = None, sorting_variable = None, runs_text = None, local_cut= False,
    recover=recover, save=save, display=display):  
    
    # If one of the variables needs to be plotted in single plots
    # then we need to retrieve the data to create the plots and h5py files
    generate_simple = False
    for gen_simple in variables.values():
        if gen_simple:
             generate_simple = True
             break
    for gen_simple in scatter_variables.values():
        if gen_simple:
             generate_simple = True
             break
    
    
    ## Generate the simple plots ------------------------
    if generate_simple:
        for runs in groups_runs:
            dTels_runs,_ = ra.runs_analysis(runs,path_file, max_residual=100, recover=recover, save=save, display=display,local_cut=local_cut, sigmas = sigmas)
            for variable,gen_simple in variables.items():
                if gen_simple:
                    analysis_variable_single(variable,dTels_runs, tels_before,tels_after,sigmas=sigmas)
            for variable,gen_simple in scatter_variables.items():
                if gen_simple:
                    if type(variable) is tuple:
                        tels = tels_after
                    else:
                        tels = l_groups_2Tels
                    _, range_a, _, n_bins_a = get_config_plot_list(variable,runs,version_a) 
                    
                    ra.hist_2D(dTels_runs,variable,tels,range_var=range_a,n_bins = n_bins_a,log_scale = False)
            del dTels_runs
        

    ## sort the runs in ----------------------------------------------
    #    * group of same {class_variable}, sorted in {class_variable}
    #    * each group is sorted in ascending value of {sorting_variable}
    if (class_variable is not None) or (sorting_variable is not None):
        groups_runs = ra.get_group_runs(groups_runs,class_variable,sorted = True,sort_variable=sorting_variable)
       
    ## Generate the NOT super-imposed plots ------------------------
    for runs in groups_runs:
        for variable in variables.keys():
            analysis_variable_global(variable,runs,tels_before,tels_after,sigmas = sigmas,
                group_variable = class_variable, sorting_variable = sorting_variable, runs_text=runs_text)
        for variable in scatter_variables.keys():
            if type(variable) is tuple:
                tels = tels_after
            else:
                tels = l_groups_2Tels
            
            ra.hist_2D_compar(variable,runs,tels,sigmas = sigmas, log_scale = False,
            group_variable = class_variable, sorting_variable = sorting_variable, runs_text=runs_text, index_color=1)

    ## Generate the super-imposed plots -----------------------
    if class_variable is not None and len(groups_runs)>1:
        for variable in variables.keys():
            analysis_variable_global(variable,groups_runs,tels_before,tels_after,sigmas = sigmas, 
                group_variable = class_variable, sorting_variable = sorting_variable, runs_text=runs_text)


def launch_comparison_variables(list_runs, comparison_variables,sigmas):
    for variable,dVariable in comparison_variables.items():
        param = dVariable['param']
        group_tels = dVariable['groups_tels']

        if type(group_tels[0]) is not list:
            group_tels = [group_tels]
        
        for tels in group_tels:
            ra.comparison(list_runs,variable,tels = tels, char = param, sigmas = sigmas, show_runs = False)

def hit_visualisation_run(run):
    dTels,_ = ra.run_analysis(run,va.path_file, max_residual = 100, recover = True,save = True, display=True,local_cut=True)
    ra.hit_visualisation(dTels,name_run=run)

##################################################
launch_analysis(groups_runs, va.path_file,variables, tels_before, tels_after, scatter_variables,l_groups_2Tels,sigmas, 
    class_variable=class_variable, sorting_variable=sorting_variable, runs_text=runs_text, local_cut = ra.local_cut,
    recover=recover, save=save, display=display)


# dTels_runs,_ = ra.runs_analysis(['1400_w1011'],va.path_file, max_residual=100, recover=True, save=True, display=True)
# tels_before = ['0','1','2','3']
# tels_after = ['4','5','6','7','8','9','10','11']
# analysis_variable_single('residuals',dTels_runs,tels_before=tels_before,tels_after=tels_after)
# analysis_variable_global('residuals',['1400_w1011'],tels_before=tels_before,tels_after=tels_after)


launch_comparison_variables(list_runs, comparison_variables,sigmas)





