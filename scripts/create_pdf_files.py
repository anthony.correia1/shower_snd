#!/usr/bin/env python3

import sys
import os
#sys.path.append(os.path.abspath("/home/ancorrei/scifi/"))
import read_analysis_alt as ra


# ONLY WORKS WHEN THE PLOTS ARE SORTED IN ENERGY.

## Create PDF files
group_variables = [
    'residuals',
    'trk_chi2',
    'hit_channel',
    'ToT cluster value',
    'clu_size',
    'shower radius'
    # 'clu_std_u'
]

# group_runs = [
#     ['1400_w1011','1401_w1011','1402_w1011'],
#     ['1403_w1011','1404_w1011','1405_w1011']
# ]

# group_runs =[
#     ['1403_w1011','1404_w1011','1405_w1011'],
#     ['1406_w1011','1407_w1011','1408_w1011'],
#     ['1409_w1011','1410_w1011','1411_w1011'],
#     ['1412_w1011','1413_w1011','1414_w1011'],
#     ['1415_w1011','1416_w1011','1417_w1011'],
#     ['1418_w1011','1419_w1011','1420_w1011'],
# ]

ra.create_pdf('5.1','14--_withlead',group_variables, sigmas=[''])
#ra.create_pdf('4.4','14--_withlead',group_variables, sigmas=[''])