#!/usr/bin/env python3
from read_analysis_alt import *

# Step 1: retrieve the .root data and store it into a dictionnary
runs = ['1403_w1011','1404_w1011','1405_w1011']
dTels_runs,dTels_runs_tot = runs_analysis(runs, max_residual = 100, recover = True, save = True, display=True)

print(dTels_runs['1403_w1011']['1']['clu_u'])

# Step 2: Create the single plots

hist(dTels_runs,'clu_u', tels = ['4','5', '6', '7', '8', '9', '10', '11'], range_var = [0,3000])
hist_2D(dTels_runs, ['1.64485*clu_std_u','residuals'], ['4', '5', '6', '7', '8', '9', '10', '11'], name_var = ['shower radius',None])
hist_2D(dTels_runs,'1.64485*clu_std_u',[['5','4'],['7','6']], name_var = 'shower_radius')

# Step 3: multi-runs and multi-telescopes plots
## 1D hist
hist_compar('clu_u', ['1403_w1011','1404_w1011','1405_w1011'], ['4', '5', '6', '7', '8', '9', '10', '11'], index_color = 1, 
    runs_text = 'nlead1')

## 2D hist
hist_2D_compar(('1.64485*clu_std_u','residuals'),['1403_w1011','1404_w1011','1405_w1011'], ['4','5','6','7','8','9','10','11'], runs_text='nlead1', index_color=1)
hist_2D_compar('1.64485*clu_std_u',['1403_w1011','1404_w1011','1405_w1011'], [['5','4'],['7','6']], runs_text='nlead1', index_color=1)

