#!/usr/bin/env python3

import pickle
import os.path
import csv


import pandas as pd


def main():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """

    df = pd.DataFrame(columns=['run', 'status', 'overvoltage','energy', 'n_lead','trigger_freq'])
    param_runs = {}

    with open('runlist_desy.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        
        for row in csv_reader:
            try:
                run = int(row[0])
                status = str(row[1])
                ov = float(row[5])
                
                energy = float(row[2])
                try:
                    n_lead= int(row[14])
                except ValueError:
                    n_lead = 0
                trigger_freq = float(row[15])
            except (IndexError, ValueError):
                continue
            df = df.append({'run': run, 'status': status, 'overvoltage': ov,'energy':energy, 'n_lead':n_lead,'trigger_freq':trigger_freq}, 
                ignore_index=True)
            param_runs[str(run)] = {
                'overvoltage'   : ov,
                'energy'        : energy,
                'n_lead'        : n_lead,
                'trigger_freq'  : trigger_freq
            }
            
        
        df['run'].astype(int, copy=False)
        df['overvoltage'].astype(float, copy=False)
    
    df.to_csv('runlist.csv', index=False)
    with open('rundict.pickle','wb') as f:
        pickle.dump(param_runs,f)

if __name__ == '__main__':
    main()
