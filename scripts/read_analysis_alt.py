from variables import *
import uproot
import awkward
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from scipy import stats
import math
import os.path as op
from scipy.optimize import curve_fit
import scipy.signal as ss
from uncertainties import ufloat
import matplotlib as mpl
from tabulate import tabulate
from os import makedirs
import pickle
import h5py
from fpdf import FPDF
from PIL import Image
import matplotlib
import awkward1
import glob

plt.rc("text", usetex=False)
plt.ioff()

## GLOBAL VARIABLES

path_file = 'reconstructed_data/{}'.format(version)

dt = h5py.special_dtype(vlen=str) 


## ================================================================================================== ##
## ========================================= TOOL FUNCTIONS ========================================= ##
## ================================================================================================== ##

## PLOTTING ---------------------------------------------------------------------------------------------

def lighten_color(color, amount=0.5):
    """
    Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple.
    
    Examples:
    >> lighten_color('g', 0.3)
    >> lighten_color('#F034A3', 0.6)
    >> lighten_color((.3,.55,.1), 0.5)

    Stolen from Physics stack Exchange :S
    """
    import matplotlib.colors as mc
    import colorsys
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = np.array(colorsys.rgb_to_hls(*mc.to_rgb(c)))
    return colorsys.hls_to_rgb(c[0],1-amount * (1-c[1]),c[2])

def plot_axs(variable, ax,x_label, y_label,label=None,color = 'b', fontsize=11,density=False, alpha = False):
    ax.hist(variable, bins = 100, color = color, alpha = alpha, label = label, density=density)
    ax.set_xlabel(x_label, fontsize=fontsize)
    ax.set_ylabel(y_label, fontsize=fontsize)
    return ax

def setting_plot(fig):
    fig.subplots_adjust(bottom=0.1, left=0.1)
    fig.set_size_inches(15.2,8)

def get_info_run(run,name_var):
    """
    For the given run, "1401_w1011" for instance
    1. we want retrieve the original name of the run, 1401 for example
    2. we retrieve the value of the corresponding variable name_var

    name_var can be:
    - 'n_lead': number of leads
    - 'trigger_freq': frequency of the trigger
    - 'energy' : energy of the beam
    """
    origin_name_run = run[:4]

    return param_runs[origin_name_run][name_var]

def get_text_info_run(run, variables = ('n_lead','energy'), sep = ', '):

    origin_name_run = run[:4]
    variables = el_into_list(variables)
    if param_runs is not None:
        info_run = ''
        
        for variable in variables:
            if info_run != '':
                info_run += sep 
            short_variable = info_run_variables[variable]['short']
            value_variable  = str(param_runs[origin_name_run][variable])
            unit = info_run_variables[variable]['unit']
            info_run += "{} = {} {}".format(short_variable,value_variable,unit)
        
        return info_run
    else:
        return ""

def create_pdf(version_run,name_run,name_vars,group_tels = [['0','1','2','3'],['4','6','8','10'],['5','7','9','11']], sigmas=[''],text_add=['energy']):
    """
    Only work in sigma_mode = False
    We suppose that the documents are of the form:
    # - {run[0]}..{run[1]}..[...]/variable_tel0..1..2..2..3.pdf
    # - {run[0]}..{run[1]}..[...]/variable_tel0..4..6..8..10.pdf
    # - {run[0]}..{run[1]}..[...]/variable_tel0..5..7..9..11.pdf  
    Rather
    - name_run/variable_tel0..1..2..2..3_{extension}*.pdf
    - name_run/variable_tel0..4..6..8..10_{extension}*.pdf
    - name_run/variable_tel0..5..7..9..11_{extension}*.pdf 

    Allow to get multi-page PDF file of a set of variables and runs.
    /!\ All the plots must have been created before launching this function.
    This function merely merges several images into a PDF with a very convenient name.
    """

    # If the name_vars = single element, transform them into a list
    if type(name_run) is not str:
        name_run = el_into_list(name_run)
    name_vars = el_into_list(name_vars)
    sigmas = el_into_list(sigmas)
    text_add[-1] += '*'
    with Image.open(glob.glob(get_name_file(name_run,"PLOTS",'png', name_variable = name_vars[0].replace(" ","_"),tels = group_tels[0],sigmas=sigmas,text_add = text_add, comparison=True,version_run=version_run))[0]) as img:
        width, height = img.size
    
        
    pdf_file = FPDF(orientation ='L',format=(height*0.35,width*0.35)) # correct size ...
    pdf_file.set_margins(0.05,0.05)

    pdf_file.set_auto_page_break(0)

    for name_var in name_vars:
        for tels in group_tels:
            name_fig_path = get_name_file(name_run,"PLOTS",'png',name_var.replace(" ","_"),sigmas,tels,text_add = text_add, comparison=True, version_run = version_run)
            list_name_fig = glob.glob(name_fig_path)
            print(name_fig_path)
            print(list_name_fig)
            for name_fig in list_name_fig:
                pdf_file.add_page()
                pdf_file.image(name_fig)
 
    # save the PDF file
    PDF_name = op.join("results","COMPARISON","PLOTS","{}_{}[V{}].pdf".format(list_into_string(name_vars),list_into_string(name_run),version_run))
    pdf_file.output(PDF_name, "F")
    print(PDF_name+" saved")

def add_value_labels(ax, spacing=14,labelsize =12):
    """Add labels to the end of each bar in a bar chart.

    Arguments:
        ax (matplotlib.axes.Axes): The matplotlib object containing the axes
            of the plot to annotate.
        spacing (int): The distance between the labels and the bars.
    
    /!\ Retrieve from https://stackoverflow.com/questions/28931224/adding-value-labels-on-a-matplotlib-bar-chart

    """

    # For each bar: Place a label
    for rect in ax.patches:
        # Get X and Y placement of label from rect.
        y_value = rect.get_height()
        x_value = rect.get_x() + rect.get_width() / 2

        # Number of points between bar and label. Change to your liking.
        space = spacing
        # Vertical alignment for positive values
        va = 'top'

        # If value of bar is negative: Place label below bar
        if y_value < 0:
            # Invert space to place label below
            space *= -1
            # Vertically align label at top
            va = 'bottom'

        # Use Y value as label and format number with one decimal place
        if not int(y_value) == 0:
            label = "{}".format(int(y_value))

            # Create annotation
            ax.annotate(
                label,                      # Use `label` as label
                (x_value, y_value),         # Place label at end of the bar
                xytext=(0, space),          # Vertically shift label by `space`
                textcoords="offset points", # Interpret `xytext` as offset in points
                ha='center',                # Horizontally center label
                va=va,                      # Vertically align label differently for
                size = labelsize)

## GET NAMES ---------------------------------------------------------------------------------------------ù

def list_into_string(l):
    if type(l) is list or type(l) is tuple:

        return str(l).replace("[","").replace("]","").replace("'","").replace(", ","..").replace(",","..").replace(" ","_").replace(")","").replace("(","")
    else:
        return l

def get_mode(sigmas):
    return (sigmas != [''])

def try_makedirs(directory):
    try:
        makedirs(directory)
    except OSError:
        pass

def get_name_file(runs,kind_data,ext, name_variable = "", sigmas = [''],tels = [''],comparison=None,text_add = None, correlation = False, version_run = version_a):
    """
    Input:
    - name: preffix
    - runs: list of of all the runs
    - sigmas: list of sigmas
    - pathfile where the files are supposed to be saved
    - ext: extension

    Output: name of the file:
        results/{kind_data}/COMPARISON/RUN_{run1}..{run2}.. ..{runn}_sig{sigma1}.. ..{sigman}/{name}_RUN_{run1}..{run2}.. ..{runn}_sig{sigma1}.. ..{sigman}.{ext}
    or
        results/{kind_data}/SINGLE/RUN_{run}_sig{sigma}/{name}_sig{sigma}.{ext}
    """
    if sigmas is None:
        sigmas = ['']
    name_variable = list_into_string(name_variable) # convert it into a list of variable if it's a list.
    sigmas = el_into_list(sigmas)
    runs = flat_list(runs)
    ## SINGLE run / COMPARISON between runs
    if comparison is None:
        if kind_data != 'PICKLE':
            if (len(runs) == 1 or type(runs) is not list) and (len(sigmas) == 1 or type(sigmas) is not list):
                text_comp = "SINGLE"
            else:
                text_comp = "COMPARISON"
    else:
        text_comp = "COMPARISON" if comparison else "SINGLE"
    ## if sigma set to a fix value (5.0) (-> sigma_mode = False) / sigma changes (-> sigma_mode = True)
    sigma_mode = get_mode(sigmas)
    if sigma_mode:
        symb_sigma = '_sig'
    else:
        symb_sigma= ''
    
    # If we consider a given set of tels (telescopes)
    tel_mode = get_mode(tels)
    if tel_mode:
        symb_tel = '_tel'
    else:
        symb_tel = ''

    text_runs_sigmas = list_into_string(runs) + symb_sigma + list_into_string(sigmas)
    
    directory = op.join("results",version_run,kind_data)
    if kind_data != 'PICKLE': # if pickle, I won't need dictionnary with a lot of runs
        directory = op.join(directory,text_comp)
    # If data type = HDF5, the variables are stored inside the file, so should not be included in the directory path.
    if kind_data != "HDF5" and kind_data != "PICKLE":
        directory = op.join(directory,text_runs_sigmas)
        try_makedirs(directory)
    else: # in HDF5, the variables are stored inside a file {text_runs_sigmas}.hdf5
        name_variable = text_runs_sigmas 
    
    if correlation:
        directory= op.join(directory,"correlation")
        try:
            makedirs(directory)
        except OSError:
            pass
    added_text = ""
    
    if text_add is not None:
        for text in text_add:
            if text != "":
                added_text += '_'
                added_text += text
    try_makedirs(directory)
    return op.join(directory,name_variable + symb_tel + list_into_string(tels) +added_text +'.' + ext)

## RECOVER DATA ------------------------------------------------------------------------------------------

def el_into_list(e,size = 1):
    
    if (type(e) is not list) and (type(e) is not tuple):
        e = [e for i in range(size)]
    return e

def convert_1D_2D(dim_i,dim_j,axs_l):
    if dim_i > 1 and dim_j > 1:
        return axs_l
    
    a = np.ndarray(shape=(dim_i,dim_j), dtype=type(axs_l))
    if dim_i == 1:
        a[0,:]= axs_l
    if dim_j == 1:
        a[:,0] = axs_l
    return a

def recover_data_float(group_h5py,variable):
    return float(np.array(group_h5py[variable]))

def recover_data_list(group_h5py,variable):
    return np.array(group_h5py[variable])

## WRITTING DATA ------------------------------------------------------------------------------------------

def create_group(file_h5py,name_group):
    if name_group in file_h5py:
        return file_h5py[name_group]
    else:
        return file_h5py.create_group(name_group)

def create_dataset(group,name,data):
    if name in group.keys():
        del group[name]
    group.create_dataset(name,data= data)

## COMPUTING ----------------------------------------------------------------------------------------------

def get_bin_centres(bin_edges):
    return (bin_edges[:-1] + bin_edges[1:]) / 2

def compute_efficiency(total, success, interval=0.95):
    """
    Compute efficiency and lower, upper uncertainty bound.
    """

    eff = success / total
    # Clopper–Pearson confidence interval; for details see:
    # https://en.wikipedia.org/wiki/Binomial_proportion_confidence_interval
    q0 = (1.0 - interval) / 2.0
    q1 = q0 + interval
    lower = stats.beta.ppf(q0, success, total - success + 1)
    upper = stats.beta.ppf(q1, success + 1, total - success)

    return eff, eff-lower, upper-eff

def get_keys_dic(dic):
    return list(dic.keys())

def get_index_max(l):
    """
    Return the index of the max of the list
    """
    max_l = l[0]
    i_max=0
    for i in range(1,len(l)):
        if max_l < l[i]:
            max_l = l[i]
            i_max = i
    
    return i_max

## ================================================================================================== ##
## ===================================== TAKE-THE-DATA FUNCTIONS ==================================== ##
## ================================================================================================== ##

def cut(dTel, max_residual, min_cluster, max_1track, display, local_cut = True):
    """
    Cut on the dTel (for a given telescope and run)
    - |residuals| < max_residual
    - cluster_size > min_cluster
    if max_1track: evt_ntracks == 1

    If the variable display == True, print text.

    In this version of code
    - dTel_tot: only with the cun on evt_ntracks
    - dTel: with all the cuts
    
    If local_cut: actually cut on dTel
    Else: just return the corresponding filter of dTel_tot and dTel
    NB: for local_cut, the telescopes are considered independent. A cut on a telescope will not affect the global event.
    As a consequence, the number of events is telescope-dependent.
    This is not the case for global_cut, as only the filter is retrieved. It will be applied afterwards to all the telescopes.
    """
    # Filter 1 track
    if max_1track:
        filter_1track= dTel['evt_ntracks']==1
        n_tel_1track = filter_1track.sum()
        if local_cut:
            dTel_tot    =   dTel[filter_1track]
            dTel        =   dTel[filter_1track]
        if display:
            print('Nb of events with 1 track : {}'.format(n_tel_1track))
    
    # Filter residual & cluster size    
    if local_cut:
        filter_residual_clustersize = (min_cluster < dTel['clu_size'])
        dTel = dTel[filter_residual_clustersize]
        n_tel_clustersize = len(dTel)
        residuals = dTel['trk_u'] - dTel['clu_u']
        filter_residuals=  (max_residual > np.abs(residuals))
        dTel = dTel[filter_residuals]
        n_tel_residuals = len(dTel)
    else:
        filter_residual_clustersize = filter_1track & (min_cluster < dTel['clu_size'])
        n_tel_clustersize = filter_residual_clustersize.sum()
        residuals = dTel['trk_u'] - dTel['clu_u']
        filter_residuals=  (max_residual > np.abs(residuals))
        n_tel_residuals = filter_residuals.sum()

    if display:
        print("After cut clu_size > {} : {}".format(min_cluster, n_tel_clustersize))
        print("After cut |residuals| < {} : {}".format(max_residual,n_tel_residuals))

    if local_cut:
        return dTel, dTel_tot
    else:
        return filter_residuals, filter_1track

def run_analysis(run, path_file = path_file, max_residual=1, min_cluster=0, 
    max_1track =True, display = False, save = False, recover = True, local_cut = True):
    '''
    for a given path_file/run_{run}-trees.root
    Return 2 dictionnaries of the form dTels[tel] that contains the data for every telescope.
    If 
    - max_1track == 1, both dictionnaries contain only events with 1 track.
    - display == True, display the number of events before after cut
    Output:
    - 1st dictionnary dTels has undergone the cuts
        - residuals < max_residual
        - cluster_size > min_cluster
        - if max_1track: evt_ntracks == 1
    - 2nd dictionnary dTels has undergone only evt_ntracks == 1 if max_1track
    '''
    max_residual_save = max_residual
    load = False
    if recover:
        name_pickle = get_name_file(run,'PICKLE','pkl')
        try:
            with open(name_pickle,'rb') as f:
                dTels,dTels_tot = pickle.load(f)
                load = True
        except IOError:
            print("File {} probably does not exist".format(name_pickle))
        
    file = uproot.open(op.join(path_file,'run_{}-trees.root'.format(run)))
    #name_pickle = get_name('dTels.dTels_tot',[run],[''],'results/dTels/','pickle')

    
        
    #print("Loading of the file "+ name_pickle)
    
    if not load:
        dTels = {}
        dTels_tot = {}  

        if not local_cut:
            tels = []
        
        if display:
            print('==== RUN {} ==== '.format(run))

        first_telescope = True
        for key in file.keys(): # tel6 and tel7 # keys are b'tel6;1' or b'tel7;1'
            ## RECOVER DATA FOR A TEL -------------------------------
            num_tel =str(key,'utf-8').replace("tel","").replace(";1","") # n° of the telescope   
            if not local_cut:
                tels.append(num_tel)            

            # "clusters_unmatched" or "clusters_matched"
            tel_matched = file[key]["tracks_clusters_matched"]
                        
            dTels[num_tel] = awkward.Table(tel_matched.arrays(namedecode='utf-8'))
            n_tel=len(tel_matched)
            
            ## FILTERS ---------------------------------------------
            # We apply the cuts.
            if display:
                print('-- plane {} -- '.format(tel_name[num_tel]))
                print('Nb of unmatched events : {}'.format(len(file[key]["clusters_unmatched"])))
                print('Nb of matched events : {}'.format(n_tel))
            
            # if version_a=='4.4' and num_tel in ['4', '5', '6', '7', '8', '9', '10', '11']:
            #     min_cluster=4
            # if version_a=='4.5' and num_tel in ['4', '5', '6', '7', '8', '9', '10', '11']:
            #     min_cluster=1
            # if version_a=='4.6' or version_a == '4.7' and num_tel == '7':
            #     max_residual = 0.01
            else:
                max_residual=max_residual_save
            result = cut(dTels[num_tel], max_residual, min_cluster, max_1track,display, local_cut=local_cut)
            if local_cut:
                dTels[num_tel], dTels_tot[num_tel] = result
            else:
                # if global cut
                if first_telescope:
                    filter_dTels_cumul, filter_dTels_tot_cumul = result
                    first_telescope = False
                else:
                    # we compute the global cut
                    filter_dTels_cumul &= result[0]
                    filter_dTels_tot_cumul &= result[1]
        
        # If global cut
        # we apply globally the same cut to every telescope.
        if not local_cut:
            for tel in tels:
                dTels_tot[tel] = dTels[tel][filter_dTels_tot_cumul]
                dTels[tel] = dTels[tel][filter_dTels_cumul]
            if display:
                print("Number of elements in dTels_tot : {}".format(len(dTels_tot[tels[0]])))
                print("Number of elements in dTels : {}".format(len(dTels[tels[0]])))
        
        if save:
            name_pickle = get_name_file(run,'PICKLE','pkl')
            with open(name_pickle,'wb') as f:
                pickle.dump([dTels,dTels_tot],f)
    return dTels, dTels_tot

def runs_analysis(runs, path_file = path_file, sigmas = [''],
    max_residual=1, min_cluster=0, max_1track =True, display = False,
    save = True, recover = True, local_cut = True):
    """
    Input:
    - Runs to run
    - sigmas to run
    - path_file to find thems : 
        * If there are sigmas: path_file/run_{run}_sig{sigmas}-trees.root
        * If there aren't: path_file/run_{run}-trees.root
    - save: do we want to save dTel_runs in a pickle in order to retrieve it faster afterwards?
    - retrieve: retrieve the file if it is available
    - local_cut: if False, if an event is cut out in one of the SciFi planes, it is cut out in all the SciFi planes
    
    Output:
    if with sigmas:
    - dTels_runs[runs|sigmas][tels] (with cut)
    - dTels_runs_tot[runs|sigmas][tels] (without cut)
    if without sigmas:
    - dTels_runs[runs][tels] (with cut)
    - dTels_runs_tot[runs][tels] (without cut)

    - 1st dictionnary dTels has undergone the cuts
        - residuals < max_residual
        - cluster_size > min_cluster
        - if max_1track: evt_ntracks == 1
    - 2nd dictionnary dTels has undergone only evt_ntracks == 1 if max_1track

    """
    if sigmas is None:
        sigmas = ['']
    

    mode_sigma = get_mode(sigmas)
    if mode_sigma:
        text_sigma = '_sig'
    else:
        text_sigma = ''

    dTels_runs = {}
    dTels_runs_tot = {}      
    for run in runs:
        for sigma in sigmas:
            name_run = '{}{}{}'.format(run,text_sigma,sigma)
            name_dic = get_name_run(mode_sigma,run,sigma)
            result = run_analysis(name_run, path_file, max_residual,
                min_cluster, max_1track, display,save,recover, local_cut= local_cut)
            
            dTels_runs[name_dic], dTels_runs_tot[name_dic] = result 
    return dTels_runs, dTels_runs_tot

def get_keys(dTels_runs):
    """
    Input: dictionnary
    - dTels_runs[{run}|{sigma}][tel]
    or
    - dTels_runs[{run}][tel]

    Output:
    - list of runs
    - list of sigmas
    - list of tels
    """
    runs_text = list(dTels_runs.keys())
    if '|' not in runs_text[0]:
        runs = runs_text
        sigmas = ['']
    else:
        sigmas = []
        runs = []
        for run_text in runs_text:
            marker = run_text.find('|')
            run = run_text[:marker]
            sigma = run_text[marker+1:]
            if run  not in runs:
                runs.append(run)
            if sigma not in sigmas:
                sigmas.append(sigma)
    tels = list(list(dTels_runs.values())[0].keys())
    return runs, sigmas, tels

def get_name_run (sigma_mode,run,sigma,sep = '|'):
    """
    Input:
    - sigma_mode = answer to are we in sigma_mode?
    - run & sigma
    Return the key of the dictionnary dTels_runs according to sigma_mode
    """
    if sigma_mode:
        return '{}{}{}'.format(run,sep,sigma)
    else:
        return run

## ================================================================================================== ##
## ======================================= PLOTTING FUNCTIONS ======================================= ##
## ================================================================================================== ##

# Number of elements  -----------------------------------------------------------------------------------

def analysis_number_events(dTels_runs_tot,list_tels):
    """
    only with sigma_mode
    Input: dTels_runs_tot[{run}|{sigma}][{tel}]
    Output:
    - Print the table of the number of events in dTels_runs_tot for each run and tel.
    - Plot this table and save the result
    - Plot:
        #           Run 1                                       Run 2   ...
        # Tel 0     number_events as a function of sigma
        # Tel 1
        #  ...

    """

    ## Recover list of runs, sigmas and tels from dTels_runs_tot
    runs,sigmas,tels = get_keys(dTels_runs_tot)
    sigma_mode = get_mode(sigmas)

    ## Config plot
    #           Run 1   Run 2   ...
    # Tel 0
    # Tel 1
    #  ...
    fig,axs_l = plt.subplots(len(list_tels), len(runs))
    axs = convert_1D_2D(len(list_tels),len(runs),axs_l)
    ## Open file
    name_file = get_name_file(runs,"DATA",'txt','n_events',sigmas)
    file = open(name_file,'w')
    
    j = 0
    for run in runs:
        i = 0
        ## Open file for each run
        name_file_single = get_name_file(runs,"DATA",'txt','n_events',sigmas)
        name_h5py = get_name_file(run,"HDF5",'hdf5',sigmas = sigmas)
        
        file_single = open(name_file_single,'w')
        file_h5py = h5py.File(name_h5py, 'a')
        
        ## Write in these files
        # Store sgimas in h5py
        n_events = create_group(file_h5py,'n_events')
        create_dataset(n_events,'sigmas', data = np.array(sigmas,dtype=dt))
        
        # Name of the run in .txt
        for f in [file,file_single]:
            f.write("=== RUN {} === \n".format(run))
            f.write(("{} \n".format(get_text_info_run(run))))
        
        for tel in list_tels:
            if tel in tels:
                ## Define the list n_matched of number of matched events in dTels_runs_tot[run|...][tel] for every sigma 
                n_matched = []
                for sigma in sigmas:
                    name_run = get_name_run (True,run,sigma)
                    n_matched.append(len(dTels_runs_tot[name_run][tel]))
                
                ## Write the information in the files
                # h5py
                create_dataset(n_events,'n_events_tel{}', data = n_matched)
                # .txt
                for f in [file,file_single]:
                    f.write("-- Tel {} -- \n".format(tel_name[tel]))
                    f.write(tabulate({"Number of sigmas": sigmas, "Number of matched events": n_matched}, headers='keys'))
                    f.write('\n \n')
                
                ## Create the figure
                ax = axs[i,j]
                if len(runs) == 1 or len(list_tels) == 1:
                    fontsize = 13
                    labelsize = 12
                else:
                    fontsize = 8
                    labelsize = 6
                
                ax.plot(sigmas,n_matched,'b.')
                if i == len(list_tels)-1:
                    ax.set_xlabel("Number of sigmas", fontsize=fontsize)
                ax.grid(b=True, which='major', color='#666666',alpha = 0.5, linestyle='-')
                ax.tick_params(axis='y', which='major', labelsize=labelsize)
                #ax.legend(fontsize = 8)

            if j == 0:
                ax.set_ylabel("Plane {} \n # events".format(tel_name[tel]), fontsize=fontsize)
            
            if i == 0:
                ax.set_title("Run "+run)
                ax.set_title(get_text_info_run(run))
                
                i+=1
        j += 1

        ## Close the files
        file_h5py.close()
    file_single.close()
    print("n_events saved in " + name_file_single)
    
    #fig.subplots_adjust(bottom=0.1, left=0.1)
    #fig.set_size_inches(15.2,8)
    
    ## Finalise the figure
    setting_plot(fig)
    
    fig.suptitle("Number of matched events according to sigma [V{}]".format(version_a))
    fig.subplots_adjust(bottom=0.1, left=0.05, right=0.95)
    name_fig = get_name_file(runs,"PLOTS",'png','n_events',sigmas)
    plt.savefig(name_fig)
    print("Number of matched events saved in " + name_fig)
    
    ## Close the global files
    file.close()
    file_h5py.close()
    print("n_events saved in "+name_file)

    #plt.show()

def number_of_events(dTels_runs,dTels_runs_tot):
    """
    Print the number of events
    """
    if sigmas is None:
        sigmas = ['']
    runs,sigmas,tels = get_keys(dTels_runs)
    sigma_mode = get_mode(sigmas)
    name_file = get_name_file(runs,"DATA",'txt','n_events_cut',sigmas)
    file = open(name_file,'w')
    
    for run in runs:
        for sigma in sigmas:
            key_run = get_name_run(sigma_mode,run,sigma)
            name_run = get_name_run(sigma_mode,run,sigma,sep='_sig')
            file.write("=== RUN {} === \n".format(name_run))
            for tel in tels:
                file.write("-- Tel {} -- \n".format(tel_name[tel]))
                file.write("Number of events before cut: {} \n".format(len(dTels_runs_tot[key_run][tel])))
                file.write("Number of events after cut: {} \n".format(len(dTels_runs[key_run][tel])))
    file.close()


# Efficiency  ---------------------------------------------------------------------------------------------

def total_efficiency(dTels_runs, dTels_runs_tot):
    """
    only works in (not sigma_mode)
    Input : 2 dictionnaries
    - dTels_runs[run][tel] contains the data after a cut
    - dTels_runs[run][tel] contains the data before the cut

    Output:
    - write h5py file for every run, stored in RUN_{run}/efficiency/efficiency_tel{tel}
    - write h5py file for every tel, stored in RUN_{runs}/efficiency/
            * efficiency_tel{tel}
            * tels
    - Plot in the same figure:
        tel0  :  efficiency(run)
        tel1  :  efficiency(run)
        ...
    """

    ## Recover list of runs(|sigmas) and tels
    runs = list(dTels_runs.keys())
    tels = list(dTels_runs.values())[0].keys()

    ## Config figure
    fig  = plt.figure(1) 
    
    # Single (for each run)
    files_h5py = {}
    efficiencies_h5py = {}
    # Global (for all the runs together)
    name_global_h5py = get_name_file(runs,"HDF5",'hdf5')
    file_global_h5py = h5py.File(name_global_h5py,'a')
    efficiency_h5py = create_group(file_global_h5py,'efficiency')
    create_dataset(efficiency_h5py,'runs', data = np.array(runs, dtype = dt))
    

    i = 0
    for tel in tels : # over the 2 telescopes
        efficiency = []   
        for run in runs:
            efficiency.append(len(dTels_runs[run][tel])/len(dTels_runs_tot[run][tel]))
            
            ## H5PY
            if i == 0: # For the first telescope, I open the files and create the group 'efficiency'
                name_h5py = get_name_file(run,"HDF5",'hdf5')
                files_h5py[run] = h5py.File(name_h5py, 'a')
                efficiencies_h5py[run] = create_group(files_h5py[run], 'efficiency')
            
            create_dataset(efficiencies_h5py[run],'efficiency_tel{}'.format(tel), data = efficiency[-1])
            
            if i == len(tels)-1: # For the last runs (i.e. last telescope), I close the files
                files_h5py[run].close()
        
        create_dataset(efficiency_h5py, 'efficiency_tel{}'.format(tel), data = efficiency)
        
        ## Plot
        plt.grid(b=True, which='major', color='#666666',alpha = 0.5, linestyle='-')
        plt.plot(runs, efficiency, color = colors[i], linestyle = '', marker='.', label = "telescope {}".format(tel))   
        i+=1
    
    file_global_h5py.close()
    ## Finalise the plots and save them
    plt.xlabel("Runs")

    plt.ylabel(r'%s= $\frac{%s}{%s}$' %('Efficiency',"number\ of\ matched\ tracks","number\ of\ tracks"))
    plt.ylim([0, 1])
    plt.grid(b=True, which='major', color='#666666', linestyle='-',alpha = 0.5)
    plt.legend()
    plt.title ("Efficiency according to the runs [V{}]".format(version_a))
    setting_plot(fig)
    
    name_fig = get_name_file(runs,"PLOTS",'png','efficiency')
    plt.savefig(name_fig)
    print("Efficiency_tel\{tel\}(runs) saved in " + name_fig)
    plt.close()
    #plt.show()

def efficiency_channels(dTels_runs, dTels_runs_tot):
    '''
    Input:
    Input : 2 dictionnaries
    - dTels_runs[run][tel] contains the data after a cut
    - dTels_runs[run][tel] contains the data before the cut

    Output:
    - write h5py file for every run, stored in RUN_{run}/efficiency_channel/efficiency_channel_tel{tel}
    - write h5py file for every tel, stored in RUN_{runs}/efficiency/
            * efficiency_tel{tel}
            * tels
    - Plot:
        FOR EACH RUN run_{run}
            tel0  :  efficiency_channel({channel})
            tel1  :  efficiency_channel({channel})
            ...
    
    efficency_dic_channels{run}{tel, '6' or '7'}{channel}
    '''

    ## Recover lists of runs, tels
    runs = list(dTels_runs.keys())
    tels = list(dTels_runs.values())[0].keys()

    ## Open pdf file
    name_pp = get_name_file(runs,"PLOTS",'pdf','efficiency_channels')
    pp = PdfPages(name_pp)
        
    for run in runs:
        ## Confg figure
        fig,axs = plt.subplots(len(tels))
        fig.suptitle("Histograms of efficiencies of the channels for the run {} [V{}]".format(run, version_a))
        
        ## Open h5py
        name_h5py = get_name_file(run,"HDF5",'hdf5')
        file_h5py = h5py.File(name_h5py,'a')
        efficiency_channels_h5py = create_group(file_h5py,'efficiency_channels')

        i = 0
        for tel in tels:
            ## Create the histogram
            h_cut, edges    = np.histogram(dTels_runs[run][tel]['trk_channel'], bins=512, range=[0, 512])
            h_1track, _     = np.histogram(dTels_runs_tot[run][tel]['trk_channel'], bins=512, range=[0, 512])
        
            efficiency, lower, upper = compute_efficiency(h_1track, h_cut)

            ## Plot the histogram
            axs[i].errorbar(edges[:-1],efficiency, yerr = [lower,upper], ls='', marker='.')
            if i == len(tels)-1:
                axs[i].set_xlabel("Channel")
            if i == 0:
                axs[i].set_ylabel(r'%s= $\frac{%s}{%s}$' %('Efficiency',"number\ of\ matched\ tracks","number\ of\ tracks"))
            
            axs[i].tick_params(axis='x', which='major', labelsize=5)
            axs[i].margins(x=0)
            axs[i].grid(True)
            # for tick in axs[i].xaxis.get_major_ticks()[1::2]:
            #     tick.set_pad(15)
            
            ## Save the data
            if i == 0: # list of channels (same for all the telescopes)
                efficiency_channels_h5py.create_dataset("channels", data = edges[:-1])
            efficiency_channels_h5py.create_dataset("efficiency_channels_tel{}".format(tel), data = efficiency)
            efficiency_channels_h5py.create_dataset("efficiency_upper_channels_tel{}".format(tel), data = upper)
            efficiency_channels_h5py.create_dataset("efficiency_lower_channels_tel{}".format(tel), data = lower)
            i+=1
        fig.set_size_inches(15.2,8)
        fig.subplots_adjust(bottom=0.1, left=0.05, right=0.99)

        ## Save figures
        pp.savefig(fig,bbox_inches='tight')
        name_fig = get_name_file(run,"PLOTS",'png','efficiency_channels')
        plt.savefig(name_fig)
        print("Efficiency_channels_tel\{tel\}(run) saved in " + name_fig)
    
    plt.close(fig)
    pp.close()
    print("Efficiency_channels_tel\{tel\}(runs) saved in " + name_pp)
    #plt.show()

# Histograms of variable -----------------------------------------------------------------------------------

def retrieve_variable(variable,dTel,name_var = None):
    if variable == "residuals":
        var_content = dTel['trk_u']-dTel['clu_u']
        xlabel = "Residual_u = track_u - cluster_u [mm]"
    else:
        marker = variable.find('*')
        if marker != -1:
            operator = float(variable[:marker])
            var = variable[marker+1:]
            var_content = operator*dTel[var]
        else:                
            var_content = dTel[variable]
        if name_var is None:
            name_var = variable
        xlabel = name_var
    return var_content, xlabel

def correct_range(range_var,var_content):
    """
    return range_var in the form
    (range_min, range_max)
    """
    if range_var is None:
        range_min = min(var_content)
        range_max = max(var_content)
        range_var = (range_min,range_max) 
    elif (type(range_var) is tuple) or (type(range_var) is list):
        if range_var[0] is None:
            range_var = (min(var_content),range_var[1])
        if range_var[1] is None:
            range_var = (range_var[0],max(var_content))
    elif (type(range_var) is float) or (type(range_var) is int):
        range_var = (0-range_var,range_var)
    return range_var

def retrieve_config_variable(variable, **dic_configs):
    """
    Input: Dictionnary:
        key = name of the config (name_var', 'y_log_scale','fit','show_MPV','same_y_max')
        value = value of the config
    Return the values of the variables in argument, retrieved from the dictionnary "config_variable" in variables.py
    If they aren't there, return a default value 
    - if name_config == 'name_var', return variable
    - for the other: False, except for same_y_max and 'same_max' where it needs to be True by default
    """

    # if config is not specified, retrieve it from thev the dictionnary config_variables
    if variable in config_variables:
        for name_config in dic_configs.keys():
            if dic_configs[name_config] is None and name_config in config_variables[variable].keys():
                dic_configs[name_config] = config_variables[variable][name_config]
    
    for name_config in dic_configs.keys():
        if dic_configs[name_config] is None:
            if name_config == 'name_var':
                dic_configs[name_config] = variable
            elif name_config == 'same_y_max' or name_config == 'same_max':
                dic_configs[name_config] = True
            else:
                dic_configs[name_config] = False
    return tuple(dic_configs.values())

def group_runs_into_group_runs_3(group_runs):
    """
    Input
    - a list of list of runs
    Output: a list of (list of list of runs) so that
        in every group_runs, there are at most 3 runs 
    """
    too_many_runs = False
    for runs in group_runs:
        if len(runs)>3:
            too_many_runs = True
            break

    
    if too_many_runs:
        groups_runs_3 = [[[] for runs in group_runs]]
        i = 0 # index of 'runs'
        for runs in group_runs:
            j = 0
            for run in runs:
                if j//3 >= len(groups_runs_3):
                    groups_runs_3.append([[] for runs in group_runs])
                groups_runs_3[j//3][i].append(run)
                j+=1
            i+=1

    else:
        groups_runs_3 = [group_runs]
    
    return groups_runs_3

def hist_1run(dTels,variable,name_run,name_var = None, tels = None, n_bins = 100, range_var = None, 
    show_mean = None, show_MPV = None, show_nevents_bins = None, y_log_scale = None,
    fit = None, mu0 = 0, sigma0 = None):
    '''
    Input:
    - variable to plot. NB: "residuals" = track_u - cluster_u
    - name_run for which the variable will be plotted
    - lTels: list of tels
    - range_min and range_max: range of the variables
    - name_var : name of the variable to display
    - show_nevents_bins : = True to show the number of events in every bin (to use only if there aren't too many bins)
        if None:
            True if n_bins < 10

    Output:
    - Plot : for every telescope
                    run {run}, sigma {sigma}
            
    - Save in h5py file: In h5py file for every run, Store the histogram {variable} in RUN_{run}(sigma_{sigma})/{variable}/
                bins_tel{tel} (center)
                content_tel{tel} (of the bin)
                result of the fits (values_mean_tel{tel}, values_std_tel{tel}, chi2_tel{tel} and adjusted_r2_tel{tel})
                mean_tel{tel}
                MPV_tel{tel}
    
    '''

    # if config is not specified, retrieve it from thev the dictionnary config_variables
    name_var, show_mean, show_MPV, y_log_scale, fit = retrieve_config_variable(variable,
        name_var = name_var, show_mean = show_mean, show_MPV = show_MPV, y_log_scale = y_log_scale,fit = fit)
    
    if tels is None:
        tels = get_keys_dic(dTels)
    
    ## Open h5py
    name_h5py = get_name_file(name_run,"HDF5",'hdf5')
    file_h5py = h5py.File(name_h5py,'a')
    variable_h5py = create_group(file_h5py,variable)
    
    ## Parameters of the figure
    color = colors[0]
    labelsize = 18
    fontsize = 20
    text_info_run = get_text_info_run(name_run)
    for tel in tels:
        fig,ax = plt.subplots(1,1)
                
        ## Retrieve the variable
        var_content, xlabel = retrieve_variable(variable,dTels[tel],name_var)
        
        ## Re-write range in the form (range_min, range_max)
        range_var = correct_range(range_var,var_content)
        
        ## Plotting 
        if fit:
            color_hist = lighten_color(color,0.5)
        else:
            color_hist = color

        bins_content, bins_edges,_ = plt.hist(var_content.flatten(), bins = n_bins, range=range_var, color = color_hist,lw=0)
        bin_centers = get_bin_centres(bins_edges)
        ## Fit
        if fit:
            if sigma0 is None:
                sigma0 = range_var[1]/2 
            mu, sig,A,adjusted_r_squared = fit_gaussian(bins_content, bin_centers,mu0, sigma0)
            fit_content = gaussian(bin_centers,mu.nominal_value, sig.nominal_value, A.nominal_value)

        plt.xlim(range_var)
        if y_log_scale:
            plt.yscale('log', nonpositive='clip')
        
        mean =var_content.mean()
        MPV = bin_centers[get_index_max(bins_content)]

        ## Write in h5py file.    
        create_dataset(variable_h5py,"content_tel{}".format(tel), data=bins_content)
        create_dataset(variable_h5py,"bin_centers_tel{}".format(tel), data=bin_centers)
        create_dataset(variable_h5py,"mean_tel{}".format(tel), data=mean)
        create_dataset(variable_h5py,"MPV_tel{}".format(tel), data=MPV)
        if fit:
            create_dataset(variable_h5py,"mu_mean_tel{}".format(tel), data=mu.nominal_value)
            create_dataset(variable_h5py,"mu_std_tel{}".format(tel), data=mu.std_dev)
            create_dataset(variable_h5py,"sigma_mean_tel{}".format(tel), data=sig.nominal_value)
            create_dataset(variable_h5py,"sigma_std_tel{}".format(tel), data=sig.std_dev)
            create_dataset(variable_h5py,"norm_tel{}".format(tel), data=A.nominal_value)
            create_dataset(variable_h5py,"adjusted_r2_tel{}".format(tel), data=adjusted_r_squared)

        ## Labelling
        if fit:              
            label_fit = r'$\mu = %.2f \pm %.2e$'%(mu.nominal_value,mu.std_dev)
            label_fit += '\n'+ r'$\sigma = %.2f \pm %.2e $'%(sig.nominal_value,sig.std_dev)
            label_fit += '\n' +r'$R^2 = %.2f$'%(adjusted_r_squared)
            plt.plot(bin_centers, fit_content,label = label_fit, color = color)
            plt.legend(fontsize = labelsize)
        
        label_hist = ""
        if show_mean or show_MPV:
            if show_mean:
                label_hist += "Mean: {:.3f}".format(mean)
            if show_MPV:
                if show_mean:
                    label_hist += '\n'
                label_hist += "MPV: {:.3f}".format(MPV)
        else:
            label_hist = None
        plt.text(0.95,0.95,label_hist,transform = ax.transAxes,ha='right', va='top',
            bbox={'facecolor': 'white', 'alpha': 0.5}, fontsize = fontsize)

        if show_nevents_bins is None:
            if n_bins<=10:
                show_nevents_bins = True
            else:
                show_nevents_bins = False
        if show_nevents_bins:
            add_value_labels(ax,labelsize = labelsize)
        
        

        ## Set up the figure for each telescope
        plt.grid(b=True, which='major', color='#666666', linestyle='-',alpha = 0.5)
        plt.tick_params(axis='y', which='major', labelsize=labelsize)
        plt.tick_params(axis='x', which='major', labelsize=labelsize)
        plt.ylabel("# events", fontsize=fontsize)
        plt.xlabel(xlabel, fontsize=fontsize)
        plt.margins(x=0)
        
        ## Save the final plot
        plt.title("Histograms of {} for the run {} and plane {} [V{}] \n {} \n {:d} elements".format(name_var,name_run,tel_name[tel], version_a, 
            text_info_run,int(bins_content.sum())), fontsize = fontsize + 3)
        setting_plot(fig)
        plt.tight_layout()
        name_fig = get_name_file(name_run,"PLOTS",'png',name_var.replace(" ","_"),tels = tel)
        fig.savefig(name_fig)
        plt.close(fig)
        print("{} histogram for the run {} and plane {} saved in {}".format(variable,name_run,tel_name[tel], name_fig))

    file_h5py.close() 
  
def hist(dTels_runs,variable, sigmas = [''], name_var = None, tels = None, n_bins = 100, 
    range_var = None, show_mean = None, show_MPV = None, show_nevents_bins = None,
    y_log_scale = None, fit = None, mu0 = 0, sigma0 = None):
    '''
    Input:
    - dTels_runs has the usual forum dTels_runs[run][tel]
    - range_var: range of the x-axis of the 1D histogram
        - if None:-> range [min,max] of the distribution of the considered variable
        - if [None, b]-> range [min,b]
        - if [a, None]-> range [min,None]
        - if [a, b] -> range [a,b]
    - show_mean: show the mean of the distribution
    - show_MPV: show the MPV of the distribution
    - name_var : name of the variable to display
    - show_nevents_bins : show the number of events in every bin. if None, it is True of the number of bins <= 10

    Output:
    
    - Plot : 1 figure = a given run, a given sigma
                    run {run}, sigma {sigma}
            Tel 0
            Tel 1
                 ...
    - Save in h5py file: In h5py file for every run, Store the histogram variable in RUN_{run}(sigma_{sigma})/{variable}/
                bins (center)
                content (of the bin)
                MPV
                mean

    '''
    if sigmas is None:
        sigmas = ['']
    
    ## Some useful variables
    runs,sigmas,_ = get_keys(dTels_runs)
    sigma_mode = get_mode(sigmas)
    for run in runs:
        for sigma in sigmas:
            name_run = get_name_run(sigma_mode,run,sigma, sep='_sig')
            key_run = get_name_run(sigma_mode,run,sigma)
            if len(dTels_runs[key_run][tels[0]])>0:
                hist_1run(dTels_runs[key_run],variable, name_run,name_var = name_var,  tels = tels, n_bins = n_bins, 
                    range_var = range_var, show_mean = show_mean, show_MPV = show_MPV, show_nevents_bins = show_nevents_bins,
                    y_log_scale= y_log_scale, fit = fit, mu0 = mu0, sigma0 = sigma0)

def tels_into_list_tels(tels,max_tels):
    """
    input: list of telescopes
    output: if there are more than max_tels telescopes, 
    divide the list into the odd and even telescope
    and return the result.
    If there are less than max_tels telescopes, just return [tels]
    """
    list_tels = []
    if len(tels) > max_tels:
        even_tels = [tels[i] for i in range(0,len(tels),2)]
        odd_tels = [tels[i] for i in range(1,len(tels),2)]
        list_tels = [even_tels,odd_tels]
    else:
        list_tels = [tels]
    return list_tels

def hist_compar(variable, group_runs,tels=None, sigmas=[''], name_var = None, same_y_max = None, 
    show_mean = None, show_MPV = None, show_nevents_bins = None, show_1tick = True,fit=None,
    y_log_scale = False,norm = None, max_tels = 4, index_color = 0,
    group_variable = None, sorting_variable = None, runs_text = None):
    """
     /!\ analysis_hist have to be launched BEFORE so that the .h5py files are created.
        The plots are created from these data.

    Input:
    - variable: variable to plot
    - group_runs : list of runs to plot
        * A list if we want to plot each run separetly
                A = ['1011', ...]
        * a list A of lists B's if we want to plots the runs of a given list B super-imposed in the same plot
                A = [['1011','1012'],['1013',1014']]
            NB:  this is not implemented if there are several sigmas !!
    - sigmas : list of sigmas to plot
    - tels : list of telescopes to plot
    - name_var : name of the variable to display
    - index_color: integer in order to fix the color of the graph. 
        if more than {max_tels} telescopes are specified and we need 2 different images, then the index\_color is 
        automatically incremented for the next page, so that the histograms of the next page has a different color.
   
    - if norm is True: the diagrams are normalised
    - runs_text: other name for the runs (for the name of the folder where the image will be stored)

    - show_MPV or show_mean: show MPV/mean in the graph
    - show_1tick: only show the ticks of the last row of plots
    - show_n_evens_bins: show the number of events in every bins
        if not False, automatically activated if there are less than 10 bins
    - y_log_scale: y-axis in log scale
    - norm: normalise the graphs. Automatically does that if we want to super-impose the runs

    - max_tels: maximum number of tels in 1 page.
        If this number is exceeded, plot odd and even telescopes in 2 different pages.
    
    - index_color: color of the set of plots
        It is automatically incremented for the second page if the graph is longer than 1 page long,
            because there is more than "max_tels" telescopes

    - runs_text: alternative name of the run

    - group_variable and sorting_variable are 'energy', 'n_lead', or 'trigger_freq'
        * group_variable: how each group is separated (for instance, group of energy)
           -> In the title, we write globally : {group_variable} = ...
        * sorting_variable: how the runs are sorted inside the runs
           -> In the legend, we write {sorting_variable} = ...
    
    2 cases:
    - if len(run) = 1 ->  1 column
        Plot:
                    run {run}
            Tel 0
            Tel 1
            ...
    - if len(run) > 1 -> More than one column for the figure
                    run 1   run 2   ...
            Tel 0
            Tel 1
            ...
    - if len(sigmas) > 1 --> figures in the same plot

    """
    if sigmas is None:
        sigmas = ['']
    
    
    name_var, same_y_max, show_mean, show_MPV, y_log_scale,fit = retrieve_config_variable(variable,
        name_var = name_var, same_y_max = same_y_max, show_mean = show_mean, show_MPV = show_MPV, y_log_scale = y_log_scale,fit = fit)

    # Interesting variables for the title
    if group_variable is not None:
        variables_to_display = [var for var in interesting_runs_variables if var != group_variable] 
    else:
        variables_to_display = interesting_runs_variables
    # Convert simple element into a list
    group_runs = el_into_list(group_runs)
    sigmas = el_into_list(sigmas)
    sigma_mode = get_mode(sigmas)

    # If tels is not specified, we open a file and find all the available telescopes! # useful when I don't want to specify the only 4 telescopes
    if tels is None:
        name_run=get_name_run(sigma_mode,group_runs[0][0],sigmas[0],sep='_sig')
        file = uproot.open(op.join(path_file,'run_{}-trees.root'.format(name_run)))
        tels = [str(bTel,'utf-8').replace("tel","").replace(";1","") for bTel in file.keys()]
        file.close()
    else:
        tels = el_into_list(tels)
        
    
    list_tels = tels_into_list_tels(tels,max_tels)    
       

    if type(group_runs[0]) is not list: # if group_runs is not a list of list of runs:
        # convert it into a list of list of runs

        group_runs = [[run] for run in group_runs]
        super_imposed = False
        
    else:
        super_imposed = True
        if sigma_mode:
            print("The function cannot test several sigmas and as well as super-imposed runs.")
            return
    if same_y_max is None:
        same_y_max = not super_imposed

    # Will we normalise the plots?
    if norm is None:
        # We normalise the plots if not written otherwise, in the case we plot several histograms in the same plot.
        if super_imposed:
            norm = True
        else:
            norm = False

   

    # Feature of the figure
    title = "Histograms of {}".format(name_var)
    if len(group_runs) == 1 or len(tels)<2:
        # title += " for the run {}".format(runs[0])
        labelsize = 10
        titlesize = 12
    else:
        labelsize = 8
        titlesize = 8
    
    title += " [V{}]".format(version_a)

    if len(tels)<2:
        font_size_leg = 10
    elif sigma_mode or super_imposed:
        alpha = 0.5
        font_size_leg = 6
    elif len(group_runs)>3:
        font_size_leg = 6
    else:
        alpha = 1
        font_size_leg = 10
    
    if variable == 'residuals':
        xlabel = "Residual_u = track_u - cluster_u [mm]"
    else:
        xlabel = name_var
    
    # If there is too many runs, we separate the group_runs in groups of three.
    groups_runs_3 = group_runs_into_group_runs_3(group_runs)
    index_group_run = 1
    index_color_origin = index_color
    
    for group_runs in groups_runs_3:# 1 page
        y_scale_max = 0

        index_color = index_color_origin
        for tels in list_tels:
            fig,axs_l = plt.subplots(len(tels),len(group_runs))
            fig.suptitle(title)
            axs = convert_1D_2D(len(tels),len(group_runs),axs_l)

            j = 0
            k_col = index_color
            for runs in group_runs:
                ## Color of the plot
                if super_imposed:
                    # we choose a group of color for each run of runs.
                    colors_group = colors_groups[k_col%len(colors_groups)]
                    k_col_group = 0
                    color = colors_group[k_col_group]
                else:
                    if sigma_mode:
                        k_col = index_color
                    color = colors[k_col%len(colors)]
            
                for run in runs:
                        
                    
                    for sigma in reversed(sigmas):
                        ## Open the correspond .h5py file
                        name_h5py = get_name_file(run,"HDF5",'hdf5',sigmas = sigma)
                        file_h5py = h5py.File(name_h5py,'r')
                        variable_h5py = file_h5py[variable]



                        i = 0
                        for tel in tels:
                            ## Retrieve content of .h5py file
                            content = recover_data_list(variable_h5py,"content_tel{}".format(tel))
                            centers = recover_data_list(variable_h5py,"bin_centers_tel{}".format(tel))
                            if show_mean:
                                mean    = recover_data_float(variable_h5py,"mean_tel{}".format(tel))
                            if show_MPV:
                                MPV     = recover_data_float(variable_h5py,"MPV_tel{}".format(tel))
                            
                            
                            if fit :
                                mu_mean = recover_data_float(variable_h5py,"mu_mean_tel{}".format(tel))
                                mu_std = recover_data_float(variable_h5py,"mu_std_tel{}".format(tel))
                                sigma_mean = recover_data_float(variable_h5py,"sigma_mean_tel{}".format(tel))
                                sigma_std = recover_data_float(variable_h5py,"sigma_std_tel{}".format(tel))
                                A = recover_data_float(variable_h5py,"norm_tel{}".format(tel))
                                adjusted_r2 = recover_data_float(variable_h5py,"adjusted_r2_tel{}".format(tel))
                                
                            ## Plotting
                            # LABELS
                            label_fit = None
                            label_hist = ""
                            # if not fit and super_imposed:
                            #     if sorting_variable is None:
                            #         label_hist += "run {} ".format(run)
                            #     else:
                            #         label_hist += get_text_info_run(run,sorting_variable) + '\n'
                            if sigma_mode:
                                label_hist += r'$\ \sigma_s = {}$'.format(sigma)
                                
                                
                            if show_mean or show_MPV or fit:
                                if sigma_mode:
                                    label_hist += r'$\rightarrow$'
                                if fit:
                                    label_fit = ""
                                    if super_imposed:
                                        if sorting_variable is None:
                                            label_fit += "run {} ".format(run)
                                        else:
                                            label_fit += get_text_info_run(run,sorting_variable) + ": "                                        
                                    if sigma_mode:
                                        label_hist += r'$\sigma = %.2f, R^2 = %.2f$'%(sigma_mean,adjusted_r2)
                                    else:
                                        label_fit += r'$\sigma = %.2f \pm %.2e, R^2 = %.2f $'%(sigma_mean,sigma_std,adjusted_r2)
                                    
                                if show_mean:
                                    label_hist += "Mean: {:.3f}".format(mean)
                                if show_MPV:
                                    if show_mean:
                                        label_hist += ', '
                                    label_hist += "MPV: {:.3f}".format(MPV)
                                
                                

                            if label_hist == "":
                                label_hist = None
                            if label_fit == "":
                                label_fit = None
                            ## Plotting
                            n_events = int(content.sum())
                            if norm: # NORMALISATION
                                content = content/n_events
                                if fit:
                                    A = A/n_events           
                            axs[i,j].bar(centers, content, centers[1]-centers[0], alpha = 0.6, label = label_hist, color = lighten_color(color),lw=0)
                            
                            if fit:
                                axs[i,j].plot(centers, gaussian(centers,mu_mean, sigma_mean, A),
                                    label = label_fit, color = color,alpha=0.5)
                            
                            axs[i,j].grid(b=True, which='major', color='#666666', linestyle='-', alpha = 0.5)
                            axs[i,j].margins(x=0)
                            

                            # TITLE
                            if i == 0 :
                                if not sigma_mode:
                                    title_nb_elem = '{:d} elements'.format(int(n_events))
                                else:
                                    title_nb_elem = ''                                
                                sub_title = ""
                                if group_variable is not None:
                                    
                                    sub_title = get_text_info_run(run,[group_variable]) + '\n'
                                
                                if not super_imposed: # there is only 1 run / runs !!
                                    text_info_run = get_text_info_run(run,variables_to_display)
                                    sub_title += "Run {} : ".format(run) + text_info_run
                                    if not super_imposed:
                                        sub_title += '\n' + title_nb_elem
                                    
                                    if variable == '1.64485*clu_std_u' and not super_imposed and tel not in ['0','1','2','3']:
                                        RM = 0.0265*5.612*get_info_run(run,'n_lead')* (82 + 1.2) 
                                        text_RM = '\n RM = {:.2f}'.format(RM)
                                    else:
                                        text_RM = ""
                                    sub_title += text_RM
                                    sub_title += '\n'
                                    axs[i,j].set_title(sub_title, fontsize = titlesize)
                                    
                                else:
                                    if run == runs[0]:
                                        axs[i,j].text(0.5, 1.2+0.1*len(runs),sub_title,
                                        verticalalignment='top', horizontalalignment='center',
                                        transform=axs[i,j].transAxes,
                                        color='black', fontsize=titlesize)
                                    text_info_run = get_text_info_run(run,variables_to_display)
                                    sub_title_run = "Run {} : ".format(run) + text_info_run
                                    axs[i,j].text(0.5, 1.2+0.1*k_col_group,sub_title_run,
                                        verticalalignment='top', horizontalalignment='center',
                                        transform=axs[i,j].transAxes,
                                        color=color, fontsize=titlesize)
                            # else:
                            #     if not super_imposed and (tel in ['0','1','2','3']) :
                            #         axs[i,j].set_title(title_nb_elem + '\n', fontsize = titlesize)
                                
                                
                            # AXIS
                            if y_log_scale:
                                axs[i,j].set_yscale('log', nonpositive='clip')
                            if sigma_mode:
                                axs[i,j].tick_params(axis='y', which='major', labelsize=labelsize)

                            if i == len(tels)-1 and run == runs[0]: # last row, first curve
                                axs[i,j].set_xlabel(xlabel, fontsize=labelsize)

                            if i != len(tels)-1 and run == runs[0]:
                                if show_1tick:
                                    axs[i,j].xaxis.set_ticklabels([])
                            
                            
                            # LEGEND
                            if ((label_hist is not None) or (label_fit is not None)) and run == runs[-1]:
                                if sigma_mode:
                                    axs[i,j].legend(fontsize = font_size_leg)
                                else:
                                    axs[i,j].legend(fontsize = font_size_leg,loc="upper center", bbox_to_anchor=(0.5, 1.15), ncol = 2)
                                # else: 
                                #     axs[i,j].legend(fontsize = font_size_leg, ncol = 2)
                                    
                            if j == 0: # first column
                                if norm:
                                    norm_text = '\n normalised'
                                else:
                                    norm_text = ""
                                
                                
                                axs[i,j].set_ylabel("Plane {} \n # events {}".format(tel_name[tel],norm_text), fontsize=labelsize)


                            # Show the number of elements of the bin if there are not too many bins (less than 10 ...)
                            if show_nevents_bins is None:
                                if len(centers)<=10:
                                    show_nevents_bins = True
                                else:
                                    show_nevents_bins = False
                            if show_nevents_bins:
                                add_value_labels(axs[i,j])
                            i+=1
                            if same_y_max:
                                y_scale_max = max(y_scale_max,content.max()*1.1)
                        
                        file_h5py.close()
                        if sigma_mode:
                            k_col += 1
                            color = colors[k_col%len(colors)]
                    if super_imposed:
                        k_col_group += 1
                        color = colors_group[k_col_group%len(colors_group)]
                
                
                j += 1

            if not sigma_mode:
                k_col += 1

            # Put the same y_max for every run
            if same_y_max:
                for i in range(len(axs)):
                    for j in range(len(axs[i])):
                        axs[i,j].set_ylim(top = y_scale_max)
            setting_plot(fig)

            ## Save the image            
            fig.subplots_adjust(bottom=0.1, left=0.05, right=0.99)
            plt.tight_layout()
            text_super_imposed = 'SI' if super_imposed else ''
            if runs_text is None:
                runs_text = group_runs
            
            part_text  = "part{}".format(index_group_run) if len(groups_runs_3)>1 else ""

            if group_variable is not None:
                group_var_text = group_variable
                if not super_imposed:
                    group_var_text += str(get_info_run(group_runs[0][0],group_variable))
            else:
                group_var_text = ""
            
            name_fig = get_name_file(runs_text,"PLOTS",'png',name_var.replace(" ","_"),sigmas,tels,text_add = [text_super_imposed,group_var_text,part_text], comparison=True)
            fig.savefig(name_fig,dpi = 200)
            plt.close(fig)
            print("{} histogram for all the runs saved in {}".format(variable,name_fig))

            index_color += 1
        index_group_run += 1


# Scatter plots between tels -------------------------------------------------------------------------------

def el_into_list_of_two(variable):
    """
    We put variable in the form of a list of 2 identical variables
    """
    if type(variable) is not list:
        variable = [variable,variable]
    return variable

def hist_2D_1run(dTels,variables, name_run, l_groups_tels,name_var = None,n_bins = None,range_var=None, 
    log_scale = None):
    '''
    Input:
    - dTels_runs has the usual forum dTels_runs[run][tel]
    - variable we are interested in
        * 1 variable
        * or a list of 2 variables [variable1, variable2]
    - l_groups_tels: 
        * list of groups of 2 telescopes
            [['0','1'],['2','3'],...]
        * list of tels
            ['0','1',...]
    - name_var: alternative name for the variable -for the title and the axis label)

    Output:
    - Plot : for every telescope
                        run {run}, sigma {sigma}
        variable[tel0]
                        variable[tel1]
            
    - Save in h5py file: In h5py file for every run, Store the histogram {variable} in RUN_{run}(sigma_{sigma})/{variable}/
                values_tel0
                values_tel1
                ...
    
    '''
    
    two_variables = (type(variables) is list) or (type(variables) is tuple)
    one_variable = not two_variables
    two_telescopes = type(l_groups_tels[0]) is list

    if (two_variables and two_telescopes) or (one_variable and not two_telescopes) :
        print("2 possibles cases")
        print("- Case 1: We want to plot the 2D histogram of 2 different variables")
        print("- Case 2: We want to plot the 2D histogram of 1 variable, but for 2 different telescope.")
        print("you are not in one of these two possible cases")
        return -1

    # We put some variables in the form of a list
    variables = el_into_list(variables)
    name_var = el_into_list(name_var,len(variables))
    log_scale = el_into_list(log_scale,len(variables))
    range_var = el_into_list(range_var,len(variables))
    

    if n_bins is None or n_bins == [None,None]:
        n_bins = [50,50]
    
    if one_variable and len(range_var)==2 :
        range_var=[range_var]
    n_bins = el_into_list_of_two(n_bins)

    name_vars = []
    log_scales = []

    for i in range(len(variables)):
        name_var_retrieved, log_scale_retrieved = retrieve_config_variable(variables[i],
            name_var = name_var[i], log_scale = log_scale[i])
        name_vars.append(name_var_retrieved)
        log_scales.append(log_scale_retrieved)
    # We put l_groups_tels in the form of a list -> Formatted
    lF_groups_tels = [None for i in range(len(l_groups_tels))]
    for i in range(len(lF_groups_tels)):
        lF_groups_tels[i]=el_into_list(l_groups_tels[i])
    

    ## Open h5py
    name_h5py = get_name_file(name_run,"HDF5",'hdf5')
    file_h5py = h5py.File(name_h5py,'a')
    variables_h5py = []
    for variable in variables:
        variables_h5py.append(create_group(file_h5py,variable))

    ## Parameters of the figure
    labelsize = 14
    fontsize = 16
    text_info_run = get_text_info_run(name_run)
    
    for group_2tels in lF_groups_tels:  

        fig,_ = plt.subplots(1,1)

        labels=[]
        vars_content = []
        for i in range(2):
            if two_telescopes:
                tel = group_2tels[i]
            else:
                tel = group_2tels[0]
            ## retrieve the variable
            if two_variables:
                variable = variables[i]
                name_var = name_vars[i]
            else:
                variable = variables[0]
                name_var = name_vars[0]
            
            var_content, label= retrieve_variable(variable,dTels[tel],name_var)
            vars_content.append(var_content)
            labels.append(label)
            vars_content[i] = vars_content[i].flatten()
            if not two_variables and i==1:
                labels[i]=''
            if two_telescopes or i== 1:
                labels[i] += "\n Tel {}".format(tel_name[tel])
        

        ## Re-write range in the form (range_min, range_max)
        for i in range(len(variables)):
            range_var[i] = correct_range(range_var[i],vars_content[i])
        if one_variable:
            range_var = [range_var[i],range_var[i]]    
        ## Plotting 
        _,_,_,h=plt.hist2d(vars_content[0],vars_content[1], range=range_var, bins = n_bins)
        plt.colorbar(h)
        if one_variable and log_scales[0]:
            plt.xscale('log', nonpositive='clip')
            plt.yscale('log', nonpositive='clip')
        elif two_variables:
            if log_scales[0]:
                plt.xscale('log', nonpositive='clip')
            if log_scales[1]:
                plt.yscale('log', nonpositive='clip')

        

        ## Write in h5py files          
        if two_variables:
            for i in range(2):
                create_dataset(variables_h5py[i],"raw_content_tel{}".format(group_2tels[0]), data=vars_content[i])
                create_dataset(variables_h5py[i],"n_bins_tel{}".format(group_2tels[0]), data=n_bins[i])
                create_dataset(variables_h5py[i],"range_tel{}".format(group_2tels[0]), data=range_var[i])
        else: # only one variable (and 2 telescopes)
            create_dataset(variables_h5py[0],"raw_content_tel{}".format(group_2tels[0]), data=vars_content[0])
            create_dataset(variables_h5py[0],"raw_content_tel{}".format(group_2tels[1]), data=vars_content[1])
            create_dataset(variables_h5py[0],"n_bins_tel{}-{}".format(group_2tels[1],group_2tels[0]), data=n_bins[0])
            create_dataset(variables_h5py[0],"range_tel{}-{}".format(group_2tels[1],group_2tels[0]), data=range_var[0])

        ## Labelling
        ## Set up the figure for each telescope
        plt.grid(b=True, which='major', color='#666666', linestyle='-',alpha = 0.5)
        plt.tick_params(axis='y', which='major', labelsize=labelsize)
        plt.ylabel(labels[1], fontsize=fontsize)
        plt.xlabel(labels[0], fontsize=fontsize)
        plt.margins(x=0)
        
        ## Save the final plot
        if one_variable:
            text_variable = name_vars[0]
        else:
            text_variable = "{} vs {}".format(name_vars[0],name_vars[1])
        if two_telescopes:
            text_telescope = "planes {} - {}".format(tel_name[group_2tels[1]],tel_name[group_2tels[0]])
        else:
            text_telescope = "plane {}".format(tel_name[group_2tels[0]])
        plt.title("2d histogram of {} for the run {} and {} [V{}] \n {} \n {} elements".format(text_variable,name_run,
            text_telescope, version_a, text_info_run, len(vars_content[0])), fontsize = fontsize + 3)
        if one_variable:
            plt.axis('scaled')
        setting_plot(fig)
        plt.tight_layout()
        name_fig = get_name_file(name_run,"PLOTS",'png',list_into_string(name_vars).replace(" ","_"),tels = group_2tels,correlation = True)
        fig.savefig(name_fig)
        plt.close(fig)
        print("{} histogram for the run {} and {} saved in {}".format(text_variable,name_run,text_telescope, name_fig))
    file_h5py.close() 

def hist_2D(dTels_runs,variable,l_groups_tels, sigmas = [''],name_var = None, n_bins = 40,range_var = None, log_scale=None):
    '''
    Input:
    - dTels_runs has the usual forum dTels_runs[run][tel]
    2 cases must be considered:
        - **Case 1**: We want to plot the 2D histogram of 2 different variables
        - **Case 2**: We want to plot the 2D histogram of 1 variable, but for 2 different telescope.
    Case 1:
        - variable = list of 2 variables we are interested in
        - l_groups_tels: list of telescopes
    CASE 2:
        - variable = the variable under study
        - l_groups_tels: list of groups of 2 telescopes
    - name_var: alternative name for the variable -for the title and the axis label)
    - sigmas: list of sigmas
    - range_var=[range_var1,range_var2]: range of the x and y-axis of the 2D histogram
        range_var1 and range_var2 works in the same as as range_var in hist (for 1D histogram) 
        - if None:-> range [min,max] of the distribution of the considered variable
        - if [None, b]-> range [min,b]
        - if [a, None]-> range [min,None]
        - if [a, b] -> range [a,b]

    Output:
    
    - Plot : 1 figure = a given run, a given sigma
                        run {run}, sigma {sigma}
        variable Tel 0
                        variable Tel 1
                 ...
        if there are 2 variables and 1 telescope
                        run {run}, sigma {sigma}
        variable 1 Tel 0
                        variable 2 Tel 0
                    ...
    - Save in h5py file: In h5py file for every run, Store the histogram variable in RUN_{run}(sigma_{sigma})/{variable}/
                raw_content

    '''
    if sigmas is None:
        sigmas = ['']
    ## Some useful variables
    runs,sigmas,_ = get_keys(dTels_runs)
    sigma_mode = get_mode(sigmas)
    for run in runs:
        for sigma in sigmas:
            name_run = get_name_run(sigma_mode,run,sigma, sep='_sig')
            key_run = get_name_run(sigma_mode,run,sigma)
            result = hist_2D_1run(dTels_runs[key_run],variable, name_run, l_groups_tels, name_var = name_var, range_var = range_var, n_bins = n_bins,
                log_scale = log_scale)
            if result == -1:
                return None
            
def hist_2D_compar(variables, runs,l_groups_tels, sigmas=[''], name_var = None, log_scale = None, same_max = None, 
    index_color = 0, group_variable = None, sorting_variable = None, runs_text = None, max_tels=4):
    """
    Input:
    - variable we are interested in
        * a list of 2 variables
        or
        * one variable
    - l_groups_tels: list of groups of 2 telescopes or list of telescopes
    - name_var: alternative name for the variable (in the title, the axis label and the name the saved picture)
    - sigmas: list of sigmas
    - runs : list of runs to plot

    Case 1:
        - variable = list of 2 variables we are interested in
        - l_groups_tels: list of telescopes
    CASE 2:
        - variable = the variable under study
        - l_groups_tels: list of groups of 2 telescopes

    Output:
    
    
    - index_color: color of the set of plots
        It is automatically incremented for the second page if the graph is longer than 1 page long,
            because there is more than "max_tels" telescopes


    - group_variable and sorting_variable are 'energy', 'n_lead', or 'trigger_freq'
        * group_variable: how each group is separated (for instance, group of energy)
           -> In the title, we write globally : {group_variable} = ...
        * sorting_variable: how the runs are sorted inside the runs
           -> In the legend, we write {sorting_variable} = ...
    
    Plot:
                                        runs[0]                         runs[1]             ...
        variable l_groups_2Tels[0][0]
                                        variable l_groups_2Tels[0][1]
        variable l_groups_2Tels[1][0]
                                        variable l_groups_2Tels[1][1]    
            
            ...

        if there are 2 variables and 1 tel
                                        runs[0]                         runs[1]             ...
        variable[1] l_groups_2Tels[0]
                                        variable[0] l_groups_2Tels[0]
        variable[1] l_groups_2Tels[1]
                                        variable[0] l_groups_2Tels[1]    
            
            ...
    
    if len(sigmas) > 1 --> figures in the same plot

    """
    if sigmas is None:
        sigmas = ['']
    
    two_variables = (type(variables) is list) or (type(variables) is tuple)
    one_variable = not two_variables
    two_telescopes = type(l_groups_tels[0]) is list

    # We put l_groups_tels in the form of a list -> Formatted
    lF_groups_tels = [None for i in range(len(l_groups_tels))]
    for i in range(len(lF_groups_tels)):
        lF_groups_tels[i]=el_into_list(l_groups_tels[i])

    # We put some variables in the form of a list
    variables   = el_into_list(variables)
    name_var    = el_into_list(name_var,len(variables))
    log_scale   = el_into_list(log_scale,len(variables))
    same_max    = el_into_list(same_max,len(variables))
    
    
    name_vars = []
    log_scales = []
    same_maxs = []
    for i in range(len(variables)):
        name_var_retrieved, y_log_scale_retrieved, same_max_retrieved = retrieve_config_variable(variables[i],
            name_var = name_var[i], y_log_scale = log_scale[i], same_max = same_max[i])
        name_vars.append(name_var_retrieved)
        log_scales.append(y_log_scale_retrieved)
        same_maxs.append(same_max_retrieved)
    
    # Interesting variables for the title
    if group_variable is not None:
        variables_to_display = [var for var in interesting_runs_variables if var != group_variable] 
    else:
        variables_to_display = interesting_runs_variables
    # Convert simple element into a list
    runs = el_into_list(runs)
    sigmas = el_into_list(sigmas)
    sigma_mode = get_mode(sigmas)


    # if there are too many telescopes, create 2 different figures
    sub_l_groups_tels = tels_into_list_tels(lF_groups_tels,max_tels)
    
    # Feature of the figure
    if len(runs) == 1:
        # title += " for the run {}".format(runs[0])
        labelsize = 10
        titlesize = 12
    else:
        labelsize = 8
        titlesize = 8
    fontsize = 8

    if sigma_mode:
        font_size_leg = 8
    elif len(runs)>3:
        font_size_leg = 8
    else:
        font_size_leg = 10

    k_col = index_color
    for l_groups_tels_i in sub_l_groups_tels:
        fig,axs_l = plt.subplots(len(l_groups_tels_i),len(runs),constrained_layout=True)
        # I will plot the plots afterwards, after fixing the range, so I need to save everything
        h_save = [[[] for j in range(len(runs))] for i in range(len(l_groups_tels_i))]
        label_hist = [[[] for j in range(len(runs))] for i in range(len(l_groups_tels_i))]
        x_edges_hist = [[[] for j in range(len(runs))] for i in range(len(l_groups_tels_i))]
        y_edges_hist = [[[] for j in range(len(runs))] for i in range(len(l_groups_tels_i))]
        axs = convert_1D_2D(len(l_groups_tels_i),len(runs),axs_l)

        j = 0
        range_var_global = [[None] for l in range(len(variables))] # 2 variables
        range_var_created = [False for l in range(len(variables))]
        clim_global_created = False
        
        
        for run in runs:
            for sigma in reversed(sigmas):
                ## Open the correspond .h5py file
                name_h5py = get_name_file(run,"HDF5",'hdf5',sigmas = sigma)
                file_h5py = h5py.File(name_h5py,'r')
                variables_h5py = []
                for variable in variables:
                    variables_h5py.append(file_h5py[variable])

                i = 0
                for group_tels in l_groups_tels_i:
                    ## Retrieve content of .h5py file
                    variables_content = []
                    n_bins = []
                    range_var = []
                    labels = ["" for l in range(2)]
                    for l in range(2):
                        tel = group_tels[l] if two_telescopes else group_tels[0]
                        variable_h5py = variables_h5py[l] if two_variables else variables_h5py[0]
                        
                        variables_content.append(recover_data_list(variable_h5py,"raw_content_tel{}".format(tel)))
                        if j == 0:
                            if two_variables:
                                labels[l]=name_vars[l]
                            elif  l==1: # there is only 1 variable
                                labels[l]=name_vars[0]
                        
                            if two_telescopes or l== 1:
                                labels[l] += "\n Tel {}".format(tel_name[tel])

                    if two_variables:
                        for variable_h5py in variables_h5py: # two variables but 1 telescope
                            n_bins.append(int(recover_data_float(variable_h5py,"n_bins_tel{}".format(group_tels[0]))))
                            range_var.append(recover_data_list(variable_h5py,"range_tel{}".format(group_tels[0])))
                    else: # one variable but 2 telescopes
                        n_bins.append(int(recover_data_float(variables_h5py[0],"n_bins_tel{}-{}".format(group_tels[1],group_tels[0]))))
                        range_var.append(recover_data_list(variables_h5py[0],"range_tel{}-{}".format(group_tels[1],group_tels[0])))
                    
                    ## Plotting
                    # LABELS
                    label_hist[i][j] = ""

                    if sigma_mode:
                        label_hist[i][j] += r'$\ \sigma_s = {}$'.format(sigma)
                    if label_hist[i][j] == "":
                        label_hist[i][j] = None
                    
                    
                    ## Plotting
                    n_events = int(len(variables_content[0]))
                    
                    if one_variable:
                        range_var_plot = [range_var[0],range_var[0]]
                        n_bins_plot = [n_bins[0],n_bins[0]]
                    else:
                        range_var_plot = range_var
                        n_bins_plot = n_bins
                    
                    h ,x_edges,y_edges= np.histogram2d(variables_content[0],variables_content[1],range = range_var_plot, bins = n_bins_plot)
                    x_edges_hist[i][j] = x_edges
                    y_edges_hist[i][j] = y_edges
                    #h_save[i][j]=h
                    h_save[i][j]=variables_content
                    # _,_,_,im = axs[i,j].hist2d(variables_content[1], variables_content[0],range = range_var_plot, bins = n_bins_plot)
                    # fig.colorbar(h, ax=axs[i,j])
                        
                    axs[i,j].grid(b=True, which='major', color='#666666', linestyle='-', alpha = 0.5)
                    axs[i,j].margins(x=0,y=0)
                    if one_variable:
                        axs[i,j].set_aspect('equal')
                    else:
                        axs[i,j].set_aspect('auto')

                    # TITLE
                    if i == 0 :
                        title_nb_elem = '{:d} elements'.format(int(n_events))
                        sub_title = ""
                        if group_variable is not None:
                            
                            sub_title = get_text_info_run(run,[group_variable]) + '\n'
                        
                        text_info_run = get_text_info_run(run,variables_to_display)
                        sub_title += "Run {} \n ".format(run) + text_info_run
                        sub_title += '\n' + title_nb_elem
                        sub_title += '\n'
                        axs[i,j].set_title(sub_title, fontsize = titlesize)
                        
                    # AXIS
                    if one_variable and log_scales[0]:
                        axs[i,j].set_yscale('log', nonpositive='clip')
                        axs[i,j].set_xscale('log', nonpositive='clip')
                    elif two_variables:
                        if log_scales[0]:
                            axs[i,j].set_yscale('log', nonpositive='clip')
                        if log_scales[1]:
                            axs[i,j].set_yscale('log', nonpositive='clip')
                    if one_variable:
                        if same_maxs[0]:
                            if i != len(l_groups_tels_i)-1:
                                axs[i,j].xaxis.set_ticklabels([])
                            if j != 0:
                                axs[i,j].yaxis.set_ticklabels([])
                    else:
                        if same_maxs[0]: # y-axis
                            if j != 0:
                                axs[i,j].yaxis.set_ticklabels([])
                        if same_maxs[1]:
                            if i != len(l_groups_tels_i)-1:
                                axs[i,j].xaxis.set_ticklabels([])
                            

                    axs[i,j].tick_params(axis='y', which='major', labelsize=labelsize)
                    axs[i,j].tick_params(axis='x', which='major', labelsize=labelsize)
                    
                    
                            
                    axs[i,j].set_xlabel(labels[0], fontsize=fontsize)
                    axs[i,j].set_ylabel(labels[1], fontsize=fontsize)
                    # Computation of the global range of the x/y axis and of the global cmin and cmax (for the colorbar)
                    for l in range(len(variables)):
                        if same_maxs[l]:
                            axis_limi = range_var[l]
                            if not range_var_created[l]:
                                range_var_global[l] = axis_limi
                                range_var_created[l] = True 
                                                                  
                            else:
                                range_var_global[l] = [min(range_var_global[l][0],axis_limi[0]),max(range_var_global[l][1],axis_limi[1])]
                            
                    if not clim_global_created:
                        clim_global = (np.min(h),np.max(h))
                        clim_global_created = True
                    else: 
                        clim_global = [min(clim_global[0],np.min(h)), max(clim_global[1],np.min(h))]
                    i+=1

                file_h5py.close()
                if sigma_mode:
                    k_col += 1      
            j += 1
        k_col += 1
        
        # Put the same y_max for every run
        # if same_max:
        for i in range(len(axs)):
            for j in range(len(axs[i])): 
                _,_,_,im = axs[i,j].hist2d(h_save[i][j][0],h_save[i][j][1],bins = [x_edges_hist[i][j],y_edges_hist[i][j]])
                if two_variables:
                    if same_maxs[0]:
                        axs[i,j].set_xlim(range_var_global[0][0], range_var_global[0][1])
                    if same_maxs[1]:
                        axs[i,j].set_ylim(range_var_global[1][0], range_var_global[1][1])
                else:
                    if same_maxs[0]:
                        axs[i,j].set_xlim(range_var_global[0][0], range_var_global[0][1])
                        axs[i,j].set_ylim(range_var_global[0][0], range_var_global[0][1])
                ## LEGEND
                if (label_hist[i][j] is not None):
                    axs[i,j].legend(fontsize = font_size_leg,loc="upper center", bbox_to_anchor=(0.5, 1.15), ncol = 2)
        fig.colorbar(im, ax = axs[:,0], location = 'left')

        #setting_plot(fig)

        ## Save the image            
        #fig.subplots_adjust(bottom=0.1, left=0.05, right=0.99)
        #plt.tight_layout()
        if runs_text is None:
            runs_text = runs
        
        if group_variable is not None:
            group_var_text = group_variable
            group_var_text += str(get_info_run(runs[0],group_variable))
        else:
            group_var_text = ""
        
        if one_variable:
            text_variable = name_vars[0]
        else:
            text_variable = "{} vs {}".format(name_vars[0],name_vars[1])
        fig.suptitle("2d histogram of {} [V{}]".format(text_variable,version_a), fontsize = titlesize+4)
        fig.set_size_inches(15.2,8)
        name_fig = get_name_file(runs_text,"PLOTS",'png',list_into_string(name_vars).replace(" ","_"),sigmas,l_groups_tels_i,text_add = [group_var_text,'2D'], comparison=True, correlation = True)
        fig.savefig(name_fig,dpi = 200)
        plt.close(fig)
        print("{} histogram for all the runs saved in {}".format(variable,name_fig))

        index_color += 1
    
# Fits -----------------------------------------------------------------------------------------------------

def gaussian (x, mu, sig, A):
    """
    Return gaussian evaluated at x with
    - mean value mu
    - standard deviation sig
    - normalisation A
    """
    return A*np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))

def adjusted_r2(content_bins,fit_bins,n_params):
    """
    Input:
    - content_bins: content of every bin
    - fit_bins: fitted value for every bin
    - n_params: number of parameters in the fit
    """
    n_bins = len(content_bins)
    SS_Residual = np.sum(np.square(content_bins-fit_bins))       
    SS_Total = np.sum((content_bins-np.mean(content_bins))**2)     
    adjusted_r_squared = 1 - SS_Residual/SS_Total*(n_bins-1)/(n_bins-n_params-1)
    return adjusted_r_squared

def reduced_chi2(content_bins,fit_bins,n_params):
    """
    Input:
    - content_bins: content of every bin
    - fit_bins: fitted value for every bin
    - n_params: number of parameters in the fit
    """
    n_bins = len(content_bins)
    diff = np.sum(np.square(fit_bins-content_bins))
    chi2 = np.divide(diff,content_bins) # sigma_i^2 = mu_i
    reduced_chi2 = chi2/(n_bins-n_params)
    return reduced_chi2

def fit_gaussian (bins,centers, mu0, sigma0):
    """
    Fit a histogram given by (bins, centers) by a gaussian
    Input:
    - bins: bin content
    - centers: list of the central value of every bin
    - mu0 and sigma0: initiaul value of the fit

    Output: result of the fit:
        ufloat(mu),ufloat(sigma),ufloat(A), adjusted R^2
    
    """
    result, result_cov = curve_fit(gaussian, centers, bins, p0=[mu0, sigma0, bins.sum()], bounds=([-np.inf,0,0],[np.inf,np.inf,np.inf]))
    fit_bins = gaussian(centers,result[0], result[1], result[2])
    n_params = 2 # number of unkown coeffs in the model.
    adjusted_r_squared = adjusted_r2(bins,fit_bins,n_params)
    
    
    return_r = ()

    for i in range(len(result)):
        return_r +=tuple([ufloat(result[i],result_cov[i,i])])
    return_r += tuple([adjusted_r_squared])
    return return_r

# Comparison variables according to the run -----------------------------------------------------------------
def comparison(runs,variable,tels = [0], char = 'MPV', sigmas = [''],with_leads = True,show_runs=False, name_runs = None):
    '''   
    input:
    - runs: runs to compare
    - variable: what variable are we going to compare for different runs
    - tels: for what tels?
    - char: considering the distribution of the variable, are we going to compare
        * 'MPV': Most Probable Value
        * 'mean' : Mean value
        * 'sigma' : sigma of the gaussian fit
        * 'max'
    - if with_leads = True, instead of having runs in x-axis, we'll have "leads".
        In this case, if show_runs = True,
        all the name of the runs corresponding to a #lead and an energy, are are indicated below the figure. This is very quite ugly.
    Outplot:
        Plot:
        if with_leads = False
         - x: runs
         - y: MPV/mean/sigma/max of the considered variable
        if with_leads = True
         - x: # leads
         - y: MPV/mean/sigma/max of the considered variable
    
    /!\ the corresponding plots must have been created before (the MPV/mean can be computed before, but not the sigma)
    The MPV than the mean is better because it does not depend on the chosen range of the x-axis of the histogram
    '''

    if sigmas is None:
        sigmas = ['']
    

    fig,ax = plt.subplots(1)
    plt.title("{} of {} according to the runs [V{}]".format(char, variable, version_a))

    

    sigma_mode = get_mode(sigmas)

    if with_leads:
        list_nleads_E = {} # values[energy] will allow to get
        variable_values_E = {} # values[energy] will allow to get
        variable_values_err_E = {}
        list_runs_E = {}
    else:
        list_runs = []
        variable_values={}
        variable_values_err = {} # only used if char = 'sigmas'
    i=0
    for run in runs:
        for sigma in sigmas:
            name_run = get_name_run(sigma_mode,run,sigma,sep='_sig')

            ## in every dictionnary, add the entry corresponding the the given energy
            if with_leads:
                energy = get_info_run(run,'energy')
                n_lead = get_info_run(run,'n_lead')
                if energy in list_nleads_E.keys():
                    list_nleads_E[energy].append(n_lead)
                    list_runs_E[energy].append(name_run)
                else:
                    list_nleads_E[energy] = [n_lead]
                    list_runs_E[energy] = [name_run]
                    variable_values_E[energy] = {}
                    variable_values_err_E[energy] = {}
                

            else:
                list_runs.append("{} \n {}".format(name_run, 
                    get_text_info_run(run, sep = '\n')))
            
            # Open h5py file
            name_h5py = get_name_file(run,"HDF5",'hdf5',sigmas = sigma)
            file_h5py = h5py.File(name_h5py,'r')
            variable_h5py = file_h5py[variable]

            for tel in tels:
                if with_leads:
                    if tel not in variable_values_E[energy]:
                        variable_values_E[energy][tel] = []
                        variable_values_err_E[energy][tel] = [] # only used if char = 'sigmas'
                    variable_values_list = variable_values_E[energy][tel]
                    variable_errors_list = variable_values_err_E[energy][tel] # only used if char = 'sigmas'
                    
                else:
                    if i==0  :
                        variable_values[tel] = []
                        variable_values_err[tel] = [] # only used if char = 'sigmas'
                    variable_values_list = variable_values[tel]
                    variable_errors_list = variable_values_err[tel] # only used if char = 'sigmas'

                # If the MPV/mean of the distribution is already stored, we retrieve else
                name_char_var_h5py = "{}_tel{}".format(char,tel) # MPV_tel0 for instance
                if char in ['mean','MPV']:
                    if name_char_var_h5py in variable_h5py:
                        variable_values_list.append(recover_data_float(variable_h5py,name_char_var_h5py))
                    else: # if else, we compute it
                        content = recover_data_list(variable_h5py,"content_tel{}".format(tel))
                        centers = recover_data_list(variable_h5py,"bin_centers_tel{}".format(tel))
                        if char == 'mean':
                            variable_values_list.append(content.mean())
                        elif char == 'MPV':
                            variable_values_list.append(centers[get_index_max(content)])
                elif char == 'sigma':
                    variable_values_list.append(recover_data_float(variable_h5py,"{}_mean_tel{}".format(char,tel)))
                    variable_errors_list.append(recover_data_float(variable_h5py,"{}_std_tel{}".format(char,tel)))
                elif char == 'max':
                    content = recover_data_list(variable_h5py,"content_tel{}".format(tel))
                    variable_values_list.append(content.max())

            file_h5py.close()
        i+=1
      
    k_col = 0

    if with_leads:
        for tel in tels:
            color = colors[k_col]
            j=0
            
            for energy in variable_values_E.keys():
                list_nleads = list_nleads_E[energy]
                variable_values = variable_values_E[energy][tel]
                if char == 'sigma':
                    variable_errors = variable_values_err_E[energy][tel]
                    ax.errorbar(list_nleads,variable_values, yerr = variable_errors, 
                        ls='-', marker='.', label = 'Plane {}, E = {} GeV'.format(tel_name[tel], energy), color = color)
                else:
                    ax.plot(list_nleads,variable_values, label = 'Plane {}, E = {} GeV'.format(tel_name[tel], energy), 
                        color = color, linestyle= '-', marker = '.')
                
                if show_runs:
                    trans = ax.get_xaxis_transform()
                    for i in range(len(list_nleads)):
                        
                        plt.annotate(list_runs_E[energy][i],
                            (list_nleads[i],0), # this is the point to label
                            textcoords="offset points", # how to position the text
                            xytext=(0,-30-9*4*j-9*k_col), # distance from text to points (x,y)
                            ha='center',
                            color = color,
                            xycoords=trans)
                color= lighten_color(color)
                j+=1
            k_col += 1
            xlabel = '# leads'


            
    else:
        for tel in tels:
            color = colors[k_col]
            if char == 'sigma':
                ax.errorbar(list_runs,variable_values[tel], yerr = variable_values_err[tel], 
                    ls='-', marker='.', label = 'Plane {}'.format(tel_name[tel]), color = color)

            else:
                ax.plot(list_runs,variable_values[tel], label = 'Plane {}'.format(tel_name[tel]), 
                    color = color, linestyle= '-', marker = '.')
            k_col += 1
        
        for tick in ax.xaxis.get_major_ticks()[1::2]:
            tick.set_pad(40)
        xlabel = 'runs'

    ax.set_xlabel(xlabel, fontsize = 30)
    ax.set_ylabel('{} of {}'.format(char,variable), fontsize = 30)     
    ax.grid(b=True, which='major', color='#666666',alpha = 0.5, linestyle='-')
    ax.legend(ncol = 2,fontsize = 15) 
    ax.tick_params(axis='x', which='major', labelsize=20)
    ax.tick_params(axis='y', which='major', labelsize=20)

    ## Save the final plot
    setting_plot(fig)
    name_runs = name_runs if name_runs is not None else runs
    name_fig = get_name_file(runs,"PLOTS",'png',char+'_'+variable,tels = tels, sigmas=sigmas)
    fig.savefig(name_fig,bbox_inches='tight', dpi = 200)
    plt.close()
    print("{} of {} for the runs {} saved in {}".format(char, variable, runs, name_fig))

## ================================================================================================== ##
## ============================================= SORTING ============================================ ##
## ================================================================================================== ##

def flat_list(L):
    """
    Flatten A 2D list into a 1D list.
    """
    if type(L[0]) is list:
        return [e for sub_L in L for e in sub_L]
    else:
        return L

def append_sorted(L,e):
    """
    Inputs:
    - list l
    - element e to add to the list L
    Output:
    - Insert the element at the correct position in the list
    - return its index 
    """
    if len(L)==0:
        L.append(e)
        return 0
    
    i=0
    while i<len(L) and L[i]<e:
        i+=1
    
    if i < len(L) and L[i] == e:
        return i
    else:
        # At this stage, we do have L[i]>e and L[i-1]<e
        L.insert(i,e)
    return i

def append_unsorted(L,e):
    i=0
    while i<len(L) and L[i] != e:
        i+=1
    if i == len(L):
        L.append(e)
        return len(L)-1
    else:
        return i

def append_list(L,e,sorted = False):
    if sorted:
        return append_sorted(L,e)
    else:
        return append_unsorted(L,e)

def get_group_runs(runs,variable,sorted = True,sort_variable = None):
    """
    Inputs:
    - runs: list of runs or list of list of runs
    - variable: variable that is used to sort the runs
        * 'n_lead': number of leads
        * 'trigger_freq': frequency of the trigger
        * 'energy' : energy of the beam
    - if sorted == True: groups sorted into ascending order of the value of {variable} (fun)
    - if sort_variable is not None: the runs inside every group of runs is sorted in the ascending order of {sort_variable}

    Output: return a list of group of runs
    * each group corresponds to a unique value of {variable}
    * if sorted == True : the groups are sorted into ascending order of the value of {variable}
    * if sort_variable is not None: every  group of runs is sorted in the ascending order of {sort_variable}
    """
    runs = flat_list(runs) # in case the list is not 1D ...
    second_sorting = (sort_variable is not None)
    variable_values = []
    groups_runs = []

    sort_variable_values = []

    for run in runs: 
        variable_val = get_info_run(run,variable)
        
        if second_sorting:
            sort_variable_val = get_info_run(run,sort_variable)
        
        len_variable_vals = len(variable_values)
        index = append_list(variable_values,variable_val,sorted)
        
        if len_variable_vals == len(variable_values): # if we did not have added an element to the list
            if second_sorting :
                second_index = append_list(sort_variable_values[index],sort_variable_val,second_sorting)
            else:
                second_index = len(groups_runs[index])
            groups_runs[index].insert(second_index,run)
        else:
            # new element in the list
            groups_runs.insert(index,[run])
            if second_sorting:
                sort_variable_values.insert(index,[sort_variable_val])

        
    
    return groups_runs

## ================================================================================================== ##
## ============================================ ANALYSIS ============================================ ##
## ================================================================================================== ##

def hit_visualisation(dTels, tels = ['5', '4', '7', '6', '9', '8', '11', '10'], starting_event = 0,end_event=50, name_run = None):  
    """
    This function allows, for a given run (and a set of telescopes) to visualise the hits of the events in the specified telescopes.
    Input: 
    - dTels: for a given run, dTels is a dictionnary such that dTels[tel] allows to access to the data of the telescope {tel}
    - tels: list of telescopes
    - name_run: name of the run (for the folder in which it will be saved)

    
    """
    n_events = len(dTels[tels[0]]['hit_channel'])
    list_tels = tels_into_list_tels(tels,4)  
    # x_1 x_2 x_3 x_4
    # y_1 y_2 y_3 y_4
    directory = op.join("results",version_a,"visualisation", name_run)
    try:
        makedirs(directory)
    except OSError:
        pass
    name_pp = op.join(directory,"{}-{}_events.pdf".format(starting_event,end_event))
    pp = PdfPages(name_pp)
    
    for e in range(starting_event,min(end_event,n_events)):
        y_max = 0
        fig,axs_l = plt.subplots(len(list_tels[0]),len(list_tels))
        axs = convert_1D_2D(len(list_tels[0]),len(list_tels),axs_l)
        if name_run is not None:
            text_run = " - run {}".format(name_run)
        else:
            text_run = ""
        fig.suptitle("Event {}{} [V{}]".format(e,text_run,version_a))
        for i in range(len(list_tels)):
            tels = list_tels[i]
            for j in range(len(tels)):
                tel = tels[j]
                axs[j,i].set_xlim([0,511])
                hit_channel = dTels[tel]['hit_channel'][e]
                hit_fine_value = dTels[tel]['hit_fine_value'][e]
                axs[j,i].bar(hit_channel,hit_fine_value,1)
                axs[j,i].set_title('Plane {} \n {} hits'.format(tel_name[tel],len(hit_channel)))
                axs[j,i].set_xlabel("hit channel")
                axs[j,i].set_ylabel("hit value")
                axs[j,i].grid()
                y_max = max(max(hit_fine_value),y_max)
        
        for i in range(len(list_tels)):
            for j in range(len(tels)):
                axs[j,i].set_ylim([0,y_max])
        
        fig.set_size_inches(8,15.2)
        plt.tight_layout()      

        
        #fig.savefig(name_fig,dpi = 200)
        pp.savefig(fig,bbox_inches='tight')
        plt.close(fig)
        print("event {} saved".format(e))
        #plt.show()
    pp.close()
        
# def new_barycentre(dTel,per_remove = 0.05):

#     hit_channel = dTel["hit_channel"]

#     # For every event, sort hit_channel in ascending order 
#     hit_channel_sorted = awkward1.sort(hit_channel)

#     # Remove 5% at the beginning and at the end
#     size = (awkward1.num(hit_channel_sorted)*per_remove).astype(int)


    

