#!/usr/bin/env python3

import scifi_rec as sfr
import argparse
import os.path as op
from os import makedirs
import pandas as pd

def main():
    version = '5.0'
    parser = argparse.ArgumentParser('Run reconstruction for a single run')
    parser.add_argument('runs', type=int, nargs = '+', help='Run number to be processed')
    parser.add_argument('-s','--spatial_sigma_max', type=int, nargs = '+', help='Spatial sigma max of search of clusters, to be processed')
    parser.add_argument('-r','--ref_runs', type=int, nargs = '+', help='Run number used as a reference')
    parser.add_argument('-rr','--run_ref_run', action = 'store_true', help='run the calibration and alignment of the reference run')
    parser.add_argument('-rd','--redo', action = 'store_true', help='redo the the calibration and alignment of the run')
    parser.add_argument('--verbose', '-v', action='store_true')
    args = parser.parse_args()
    if args.ref_runs is None:
        args.ref_runs = [None]
    for ref_run in args.ref_runs:
        for run in args.runs:
            if ref_run is None:
                ref_run = run
            if args.spatial_sigma_max is None:
                args.spatial_sigma_max = [None]
            for spatial_sigma_max in args.spatial_sigma_max:
                run_reconstruction(run, spatial_sigma_max = spatial_sigma_max, ref_run = ref_run,run_ref_run = args.run_ref_run, 
                redo = args.redo, verbose=args.verbose, output_path='reconstructed_data/{}'.format(version), version = version)


def run_reconstruction(run, spatial_sigma_max = None, raw_data_path='raw_data/RUN_{}', converted_data_path='converted_data', calibration_path='calibration_files', geometry_path='geometry_files', output_path='reconstructed_data',
    ref_run = None, run_ref_run = False, redo = False,version = None, **kwargs):
    """ Runs the reconstruction.

    The reconstruction is structured in the following way:
    1. calibration files are generated using sfr-calibrate and reading raw data directly (it's not necessary to convert them to root files beforehand. They are relatively small, so are quicly read by the raw txt parser and this is run one anyway)
    2. raw data files are converted to ROOT files and the calibration is applied during conversion
    3. Coarse alignemnt. A coarse alignment time based on correlations is run.
    4. Fine alignment. alignment based on local chi^2 is run and the final geometry file is generated
    5. Reconstruction. Hits are filtered here, then clusters and tracks are calculated

    """

    
    config = "analysis"
    output_name = 'run_{:04d}'.format(run)
    
    if 1311 <= run and run <= 1319:
        device = 'device_lead_outside.toml'
    elif run >= 1400 and run <= 1499:
        config += "_14"
        if version == '4.1' or version == '4.2':
            device = "device_lead_inside_clu.toml"
        elif version == '5.0' :
            device  = "device_lead_inside_ana.toml"
        else:
            device = "device_lead_inside.toml"
    else:
        device = 'device.toml'
    
    exist_ref = True
    if ref_run is None:
        ref_run = run
        run_ref_run = False
        exist_ref = False
    else:
        output_name += '_w{:04d}'.format(ref_run)

    if spatial_sigma_max is not None:
        config += '_sig{}'.format(spatial_sigma_max)
        output_name += '_sig{}'.format(spatial_sigma_max)
    config += '.toml'

    try:
        makedirs(converted_data_path)
    except OSError:
        pass
    
    try:
        makedirs(output_path)
    except OSError:
        pass
    
    ## CALIBRATION AND CONVERTION
    # For run
    if redo or not op.isfile(op.join(calibration_path, 'run_{:04d}'.format(run), 'calib_00.txt')):
        try: 
            makedirs(op.join(calibration_path, 'run_{:04d}'.format(run)))
        except OSError:
            pass
        print("CALIBRATION OF RUN {}".format(run))
        sfr.calibrate(
            raw_data_path.format('{:04d}_0'.format(run)),
            op.join(calibration_path, 'run_{:04d}'.format(run)),
            device=device,
            **kwargs
        )
 
    if redo or not op.isfile(op.join(converted_data_path, 'run_{:04d}.root'.format(run))):
        print("CONVERSION OF RUN {}".format(run))
        sfr.convert(
            raw_data_path.format('{:04d}'.format(run)),
            op.join(converted_data_path, 'run_{:04d}'.format(run)),
            op.join(calibration_path, 'run_{:04d}'.format(run), 'calib_%02d.txt'),
            device=device,
            config=config,
            **kwargs
            )
    # For run_ref
    if exist_ref and (run_ref_run or not op.isfile(op.join(calibration_path, 'run_{:04d}'.format(run),"calib_00.txt"))) :
        try: 
            makedirs(op.join(calibration_path, 'run_{:04d}'.format(run)))
        except OSError:
            pass
        print("CALIBRATION OF RUN {}".format(ref_run))
        sfr.calibrate(
            raw_data_path.format('{:04d}_0'.format(ref_run)),
            op.join(calibration_path, 'run_{:04d}'.format(ref_run)),
            device=device,
            **kwargs
        )

    if exist_ref and (run_ref_run or not op.isfile(op.join(converted_data_path, 'run_{:04d}.root'.format(ref_run)))):
        print("CONVERSION OF RUN {}".format(ref_run))
        sfr.convert(
            raw_data_path.format('{:04d}'.format(ref_run)),
            op.join(converted_data_path, 'run_{:04d}'.format(ref_run)),
            op.join(calibration_path, 'run_{:04d}'.format(ref_run), 'calib_%02d.txt'),
            device=device,
            **kwargs
        )

    ## ALIGNMENT (time and fine in space) for run and ref_run
    '''
    I do it if
    - there is not "ref_run" and I've asked to redo the alignment
    - there is a "ref_run" and I've asked to redo its aligment
    - the file doesn't exist
    '''
    if (redo and not exist_ref) or (exist_ref and run_ref_run) or not op.isfile(op.join(geometry_path, 'run_{:04d}_time-geo.toml'.format(ref_run))):
        try:
            makedirs(geometry_path)
        except OSError:
            pass
        print("ALIGNMENT OF RUN {}".format(ref_run))
        sfr.align(
            op.join(converted_data_path, 'run_{:04d}.root'.format(ref_run)),
            op.join(geometry_path, 'run_{:04d}_time'.format(ref_run)),
            subsection='time',
            num_events=1000,
            device=device,
            **kwargs
        )
        sfr.align(
            op.join(converted_data_path, 'run_{:04d}.root'.format(ref_run)),
            op.join(geometry_path, 'run_{:04d}'.format(ref_run)),
            subsection='fine',
            num_events=1000,
            device=device,
            geometry=op.join(geometry_path, 'run_{:04d}_time-geo.toml'.format(ref_run)),
            **kwargs
        )
    
    # sfr.align(
    #     op.join(converted_data_path, 'run_{:04d}.root'.format(run)),
    #     op.join(geometry_path, 'run_{:04d}_uvs'.format(run)),
    #     subsection='fine_uvs',
    #     num_events=1000,
    #     device=device,
    #     geometry=op.join(geometry_path, 'run_{:04d}_time-geo.toml'.format(run)),
    #     **kwargs
    # )
    ## RECONSTRUCTION
    print("RECONSTRUCTION OF RUN {}".format(run))
    sfr.reconstruct(
        op.join(converted_data_path, 'run_{:04d}.root'.format(run)),
        op.join(output_path, output_name),
        subsection='3d',
        skip_events=1000,
        num_events=1000000000,
        device=device,
        config=config,
        geometry=op.join(geometry_path, 'run_{:04d}-geo.toml'.format(ref_run)),
        **kwargs
    )
    
    # sfr.reconstruct(
    #     op.join(converted_data_path, 'run_{:04d}.root'.format(run)),
    #     op.join(output_path, 'run_{:04d}_time'.format(run)),
    #     subsection='3d_time',
    #     skip_events=1000,
    #     num_events=1000000000,
    #     device=device,
    #     geometry=op.join(geometry_path, 'run_1011-geo.toml'),
    #     #geometry=op.join(geometry_path, 'run_{:04d}-geo.toml'.format(run)),
    #     **kwargs
    # )


if __name__ == '__main__':
    main()
