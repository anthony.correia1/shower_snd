import pickle
import numpy as np

## VERSION =====================================================================================
version = '4.2' # version of the reconstruction
version_a = '4.3' # version of the analysis (if we want to perform another cut for instance)

local_cut = False

path_file = 'reconstructed_data/{}'.format(version)

## EXPERIMENT ==================================================================================

# Other name for the telescopes
tel_name = {
    '0'  :'1_y',
    '1'  :'1_x',
    '2'  :'2_y',
    '3'  :'2_x',
    '4'  :'3_y',
    '5'  :'3_x',
    '6'  :'4_y',
    '7'  :'4_x',
    '8'  :'5_y',
    '9'  :'5_x',
    '10' :'6_y',
    '11' :'6_x'
}



config_variables = {
    'residuals' : {
        'name_var':None,
        'y_log_scale':False,
        'fit':True,
        'show_MPV':False,
        'same_y_max':False
    },
    'hit_channel' : {
        'name_var':None,
        'y_log_scale':False,
        'fit':False,
        'show_MPV':False,
        'same_y_max':None
    },
    'clu_channel' : {
        'name_var':None,
        'y_log_scale':False,
        'fit':False,
        'show_MPV':False,
        'same_y_max':None
    },
    'clu_value' : {
        'name_var':'ToT cluster value',
        'y_log_scale':True,
        'fit':False,
        'show_MPV':True,
        'same_y_max':None
    },
    'trk_chi2':{
        'name_var':None,
        'y_log_scale':False,
        'fit':False,
        'show_MPV':True,
        'same_y_max':False
    },
    'clu_std_u':{
        'name_var':None,
        'y_log_scale':False,
        'fit':False,
        'show_MPV':True,
        'same_y_max':None
    },
    'clu_std_v':{
        'name_var':None,
        'y_log_scale':False,
        'fit':False,
        'show_MPV':True,
        'same_y_max':None
    },
    '1.64485*clu_std_u':{
        'name_var':'shower radius',
        'y_log_scale':False,
        'fit':False,
        'show_MPV':True,
        'same_y_max':None
    },
    'evt_nclusters':{
        'name_var':None,
        'y_log_scale':False,
        'fit':False,
        'show_MPV':False,
        'same_y_max':None
    },
    'clu_size':{
        'name_var':None,
        'y_log_scale':False,
        'fit':False,
        'show_MPV':False,
        'same_y_max':None
    },
    'clu_u':{
        'name_var':None,
        'y_log_scale':False,
        'fit':False,
        'show_MPV':False,
        'same_y_max':None,
    },

}


# Dictionnary of the runs
try:
    with open('rundict.pickle','rb') as f:
        param_runs = pickle.load(f)
    info_run_variables = {
        'energy': {'short':'E','unit':'GeV'},
        'n_lead': {'short':'#leads','unit':''},
        "trigger_freq": {'short':'f_trigger','unit':'Hz'}
    }
    

    interesting_runs_variables = ['energy','n_lead']
except IOError:
    print("File {} probably does not exist".format('rundict.pickle'))
    param_runs = None



## PLOTS =======================================================================================
colors = ['b','g','r','c','m','y','k','purple','darkblue','crimson','black','springgreen']

colors_groups = [
    ['b','c','violet','deepskyblue','blueviolet','teal'], # group blue-ish
    ['y','g','k','darkolivegreen','lime','greenyellow','seagreen','olive','seagreen'], # group green-ish
    ['r','m','orange','gold','chocolate','crimson','pink'] # group red-ish
]

