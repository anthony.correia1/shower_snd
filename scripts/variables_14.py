import sys
import os
import numpy as np
sys.path.append(os.path.abspath("/home/ancorrei/scifi/"))
import variables as va

## ANALYSIS ======================================================================================

## VARIABLE FOR ANALYSIS ------------------------------------------

## Some configuration variables to load the data ----------------
recover = False # Do I retrieve the runs if they are saved ?
save = False # Do I save the cut data in a dictianary ?
display= True # Do I display the information about the cuts ? (applied cuts, number of variables after the cut, ...)

## How to sort the runs ------------------------------------------
# How to group the runs in the pages
#class_variable = 'energy'
class_variable = None
# How to sort the runs in one page
#sorting_variable = 'n_lead'
sorting_variable = None
<<<<<<< HEAD
# Name of the folder where the plots with multiple runs/telescopes are saved
=======
# Name of the folder where the plots with multiple runs are saved
>>>>>>> bf20ef3c03b2d9bc293ce29b77e45a84e69f3119
runs_text=None


##  Runs ----------------------------------------
# groups_runs = [
#     ['1403_w1011','1404_w1011','1405_w1011'],
#     ['1406_w1011','1407_w1011','1408_w1011'],
#     ['1409_w1011','1410_w1011','1411_w1011'],
#     ['1412_w1011','1413_w1011','1414_w1011'],
#     ['1415_w1011','1416_w1011','1417_w1011'],
#     ['1418_w1011','1419_w1011','1420_w1011']
# ]

# groups_runs = [
#     ['1403_w1011','1404_w1011','1405_w1011'],
#     ['1406_w1011','1407_w1011','1408_w1011'],
#     ['1409_w1011','1410_w1011','1411_w1011'],
#     ['1412_w1011','1413_w1011','1414_w1011'],
#     ['1415_w1011','1416_w1011','1417_w1011'],
#     ['1418_w1011','1419_w1011','1420_w1011']
# ]

groups_runs = [['1403_w1011']]


# 1D histograms and the 2D histograms with 2 different variables and 1 telescope.
tels_before = ['0','1','2','3']
tels_after = ['4','5','6','7','8','9','10','11']
# 2D histograms of 1 variable and 2 telescopes
l_groups_2Tels = [['5','4'],['7','6'],['9','8'],['11','10']]
# Comparison
group_tels = [
    ['0','1','2','3'],
    ['4','6','8','10'],
    ['5','7','9','11']
]




# sigmas = ['5','10','20','30','40','50','60','70']
sigmas = ['']

## 1D histograms
variables = { # {variable}:{do_we_need_to_create_the_single_plot}
    # 'clu_u':True,
    # 'clu_v':True,
    # 'clu_std_u':True,
    # 'clu_std_u':True,
    # 'clu_std_v':False,
    'trk_chi2':True,
    # 'residuals' : False,
    # 'clu_channel':True,
    # 'hit_channel':True,
    # 'clu_value': False,
    # 'evt_nclusters':True,
    # 'clu_size': True,
    # '1.64485*clu_std_u': False
}

## 2D histograms:
scatter_variables = {# {variable(s)}:{do_we_need_to_create_the_single_plot}
    '1.64485*clu_std_u': True,
    # 'clu_std_u': True,
    # ('residuals','clu_size'): True
    # ('clu_size','1.64485*clu_std_u'): True,
    # ('clu_value','1.64485*clu_std_u'): True,
    # ('trk_chi2','1.64485*clu_std_u'): True,
    # ('residuals','1.64485*clu_std_u'): True,
    # ('residuals','clu_std_u'): False,
}


comparison_variables = {
    # 'residuals' : {
    #     'param':'sigma',
    #     'groups_tels': group_tels,
    # },
    # 'hit_channel' : {
    #     'param':'max',
    #     'groups_tels':group_tels
    # },
    # 'clu_value' : {
    #     'param':'MPV',
    #     'groups_tels':group_tels
    # },
    # 'clu_std_u' : {
    #     'param':'MPV',
    #     'groups_tels':group_tels
    # },
    # 'trk_chi2':{
    #     'param':'MPV',
    #     'groups_tels':['0']
    # }
}



## Comparison of different runs
list_runs = list(np.array(groups_runs).flatten())


## CONFIG OF THE PLOTS --------------------------------------------

def included(list1,list2):
    """
    check if list1 is included in list2
    """
    for l1 in list1:
        if l1 not in list2:
            return False
    return True


def get_config_plot(variable,runs,version_a):
    """
    For me, very useful function to get the parameters of the plot for a given
    - list of runs (runs)
    - variable (variable)
    - version of the analysis (version)

    OUTPUT:

    
    If generate_single is True, we give the parameters to generate the single plots (as well), i.e.
    - range of the x-axis
        * range_b: Before the lead plate
        * range_a: Afer the lead plate
    - Range of the y-axis
        * n_bins_b : number of bins for the variables before the lead (default = 100)
        * n_bins_a : number of bins for the variables after the lead(default = 100)

    ||Idea||| Suggested update: use a CSV file and read it with pandas
    """
    # default values
    n_bins_b = 100 # number of bins, for the variables before the lead
    n_bins_a = 100 # number of bins, for the variables after the lead
    range_a = (None,None)
    range_b = (None,None)
    
    if variable == 'residuals':
        if version_a == '1.0':
            ## Run 1403-1405
            if included(runs,['1403_w1011','1404_w1011','1405_w1011']):
                range_b = 50.
                range_a = 15.
            ## Run 1400-1402
            elif included(runs,['1400_w1011','1401_w1011','1402_w1011']):
                range_b = 1.0
                range_a = 0.03
        if version_a == '3.0':
            ## Run 1403-1405
            if included(runs,['1403_w1011','1404_w1011','1405_w1011']):
                range_b = 40.
                range_a = 15.
            ## Run 1400-1402
            elif included(runs,['1400_w1011','1401_w1011','1402_w1011']):
                range_b = 1.0
                range_a = 0.03
        elif version_a == '4.4' or version_a == '4.6' or version_a == '5.0' or version_a[:2] == '5.':
            ### V4.0        
            ## Run 1400-1402
            if included(runs,['1400_w1011','1401_w1011','1402_w1011']):
                range_b = 1.0
                range_a = 0.01
            ## Run 1403-1405
            elif included(runs,['1403_w1011','1404_w1011','1405_w1011']):
                range_b = 60.
                range_a = 1.
            ## Run 1406-1408
            elif included(runs,['1406_w1011','1407_w1011','1408_w1011']):
                range_b = 60.
                range_a = 1.
            ## Run 1409-1411
            elif included(runs,['1409_w1011','1410_w1011','1411_w1011']):
                range_b = 60.
                range_a = 1.
            ## Run 1412-1414
            elif included(runs,['1412_w1011','1413_w1011','1414_w1011']):
                range_b = 60.
                range_a = 1.
            ## Run 1415-1417
            elif included(runs,['1415_w1011','1416_w1011','1417_w1011']):
                range_b = 60.
                range_a = 1.
            ## Run 1418-14120
            elif included(runs,['1418_w1011','1419_w1011','1420_w1011']): 
                range_b = 60.
                range_a = 1.


        elif version_a[:2] == '4.':
            ### V4.0        
            ## Run 1400-1402
            if included(runs,['1400_w1011','1401_w1011','1402_w1011']):
                range_b = 1.0
                range_a = 0.01
            ## Run 1403-1405
            elif included(runs,['1403_w1011','1404_w1011','1405_w1011']):
                range_b = 60.
                range_a = 1.5
            ## Run 1406-1408
            elif included(runs,['1406_w1011','1407_w1011','1408_w1011']):
                range_b = 60.
                range_a = 1.5
            ## Run 1409-1411
            elif included(runs,['1409_w1011','1410_w1011','1411_w1011']):
                range_b = 60.
                range_a = 1.5
            ## Run 1412-1414
            elif included(runs,['1412_w1011','1413_w1011','1414_w1011']):
                range_b = 60.
                range_a = 1.5
            ## Run 1415-1417
            elif included(runs,['1415_w1011','1416_w1011','1417_w1011']):
                range_b = 60.
                range_a = 1.5
            ## Run 1418-14120
            elif included(runs,['1418_w1011','1419_w1011','1420_w1011']): 
                range_b = 60.
                range_a = 1.5        
    
        n_bins_b = 400
        n_bins_a = 400
    if variable == 'clu_channel' or variable == 'hit_channel':
        range_b = (0,511)
        range_a = (0,511)
        n_bins_b = 512
        n_bins_a = 512
    
    # if variable == 'analysis_clu_u':
    #     range_b = 70
    #     range_a = 70
    #     n_bins = 512
    
    if variable == 'clu_std_u':
        if version_a == '1.0':
            range_b = (0.06,0.08)
            range_a = (1,None)
            n_bins_b  = 200
            n_bins_a = 200
        elif version_a == '4.0':
            range_b = (0.06,0.08)
            range_a = (1,60)
            n_bins_b  = 200
            n_bins_a = 200
        else:
            range_b = (0.0,0.5)
            range_a = (1,None)
            n_bins_b  = 200
            n_bins_a = 200
    if variable == 'clu_std_v':
        range_b = (35,40)
        n_bins_b  = 100
        if version_a == '3.0':
            range_a = (35,40)
            n_bins_a = 1000
        elif version_a == '4.0':
            range_a = (0,25000)
            n_bins_a = 1000
    if variable == '1.64485*clu_std_u':
        range_b = [1,None]
        range_a = [1,None]
    
    if variable == 'trk_chi2':
        if version_a == '3.0':
            ## V3.0
            range_max = 10.
        
        elif version_a[:2] == '4.' or version_a[:2] == '5.':
            ## V4.0
            # Run 1400-1402

            if included(runs,['1400_w1011','1401_w1011','1402_w1011']):  
                range_max = 10.
            # Run 1403-1405
            elif included(runs, ['1403_w1011','1404_w1011','1405_w1011']):
                range_max = 0.1
        
            ## Run 1406-1408
            elif included(runs,['1406_w1011','1407_w1011','1408_w1011']):
                range_max = 0.1
            ## Run 1409-1411
            elif included(runs,['1409_w1011','1410_w1011','1411_w1011']):
                range_max = 0.1
            ## Run 1412-1414
            elif included(runs,['1412_w1011','1413_w1011','1414_w1011']):
                range_max = 0.20
            ## Run 1415-1417
            elif included(runs,['1415_w1011','1416_w1011','1417_w1011']):
                range_max = 0.20
            ## Run 1418-14120
            elif included(runs,['1418_w1011','1419_w1011','1420_w1011']): 
                range_max = 0.30 
        
        range_b = (0,range_max)
        range_a = (0,range_max)

    if variable == 'clu_value':
        ## Run 1400-1402
        if  included(runs,['1400_w1011','1401_w1011','1402_w1011']):
            range_max_b = 350
            range_max_a = 900
        ## Run 1403-1405
        elif included(runs, ['1403_w1011','1404_w1011','1405_w1011']):
            range_max_b = 350
            range_max_a = 3000
        ## Run 1406-1408
        elif included(runs,['1406_w1011','1407_w1011','1408_w1011']):
            range_max_b = 350
            range_max_a = 5000
        ## Run 1409-1411
        elif included(runs, ['1409_w1011','1410_w1011','1411_w1011']):
            range_max_b = 350
            range_max_a = 5000
        ## Run 1412-1414
        elif included(runs, ['1412_w1011','1413_w1011','1414_w1011']):
            range_max_b = 350
            range_max_a = 5000
        ## Run 1415-1417
        elif included(runs, ['1415_w1011','1416_w1011','1417_w1011']):
            range_max_b = 350
            range_max_a = 4000
        ## Run 1418-14120
        elif included(runs, ['1418_w1011','1419_w1011','1420_w1011']): 
            range_max_b = 350
            range_max_a = 3500

        range_b = (0,range_max_b)
        range_a = (0,range_max_a)

    if variable == 'evt_nclusters':
        range_b = (0,6)
        range_a = (0,6)
        n_bins_b = 6
        n_bins_a = 6

    if variable == 'clu_size':
        ## Run 1400-1402
        range_max_b = 10
        if included(runs, ['1400_w1011','1401_w1011','1402_w1011']):
            range_max_a = 100
        ## Run 1403-1405
        elif included(runs, ['1403_w1011','1404_w1011','1405_w1011']):
            range_max_a = 100
        ## Run 1406-1408
        elif included(runs, ['1406_w1011','1407_w1011','1408_w1011']):
            range_max_a = 120
        ## Run 1409-1411
        elif included(runs, ['1409_w1011','1410_w1011','1411_w1011']):
            range_max_a = 140
        ## Run 1412-1414
        elif included(runs, ['1412_w1011','1413_w1011','1414_w1011']):
            range_max_a = 140
        ## Run 1415-1417
        elif included(runs, ['1415_w1011','1416_w1011','1417_w1011']):
            range_max_a = 120
        ## Run 1418-14120
        elif included(runs, ['1418_w1011','1419_w1011','1420_w1011']): 
            range_max_a = 100

        range_b = (0,range_max_b)
        range_a = (0,range_max_a)
        n_bins_b = range_max_b
        n_bins_a = range_max_a
        
    
    if variable == 'clu_u':
        range_b = 70
        range_a = 70
        n_bins_b = 512
        n_bins_a = 512

    return range_b, range_a, n_bins_b, n_bins_a

def get_config_plot_list(variables,runs,version_a):
    """
    Same as get_config_plot but variables can be a list
    """ 
    is_list = (type(variables) is list) or  (type(variables) is tuple)
    if  is_list:
        l_range_b, l_range_a, l_n_bins_b, l_n_bins_a = [], [], [], []
        for variable in variables:
            range_b, range_a, n_bins_b, n_bins_a = get_config_plot(variable,runs,version_a)
            l_range_b.append(range_b)
            l_range_a.append(range_a)
            l_n_bins_b.append(n_bins_b)
            l_n_bins_a.append(n_bins_a)
        
        return  l_range_b, l_range_a, l_n_bins_b, l_n_bins_a
    else:
        return get_config_plot(variables,runs,version_a)

